import Vue from 'vue'
import Router from 'vue-router'
import axios from 'axios'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      name: 'login',
      path: '/login',
      component: () => import('@/components/login.vue'),
      meta: { title: '登录页' }
    },
    {
      name: 'roles',
      path: '/roles',
      component: () => import('@/components/roles.vue'),
      meta: { title: '角色选择' }
    },
    {
      path: '/home',
      component: () => import('@/components/homepage/homepage.vue'),
      meta: {
        title: '母版页'
      },
      children: [
        {
          path: '/index',
          name: '系统首页',
          component: () => import('@/components/test/HelloWorld'),
          meta: {
            title: '系统首页',
            hideclose: true,
          }
        },
        {
          path: '/tablepage',
          name: 'tablepage',
          component: () => import('@/components/tablepage/tablepage'),
          meta: {
            title: 'tablepage页',
          }
        },
        {
          path: '/basicProject',
          name: 'basicProject',
          component: () => import('@/components/basicproject/basicProject.vue'),
          meta: {
            title: '项目管理',
          }
        },
        {
          path: '/basicDatabase',
          name: 'basicDatabase',
          component: () => import('@/components/basicdatabase/basicDatabase.vue'),
          meta: {
            title: '数据库管理',
          }
        },
        {
          path: '/basicTemplate',
          name: 'basicTemplate',
          component: () => import('@/components/basictemplate/basicTemplate.vue'),
          meta: {
            title: '模板管理',
          }
        },
        {
          path: '/basicTables',
          name: 'basicTables',
          component: () => import('@/components/basictables/basicTables.vue'),
          meta: {
            title: '数据库表',
          }
        },
        {
          path: '/basicTemplateVariable',
          name: 'basicTemplateVariable',
          component: () => import('@/components/basictemplatevariable/basicTemplateVariable.vue'),
          meta: {
            title: '模板变量维护',
          }
        },
        {
          path: '/basicTemplateType',
          name: 'basicTemplateType',
          component: () => import('@/components/basictemplatetype/basicTemplateType.vue'),
          meta: {
            title: '模板类型',
          }
        },
        {
          path: '/sysUser',
          name: 'sysUser',
          component: () => import('@/components/sysuser/sysUser.vue'),
          meta: {
            title: '用户管理',
          }
        },
        {
          path: '/sysRole',
          name: 'sysRole',
          component: () => import('@/components/sysrole/sysRole.vue'),
          meta: {
            title: '角色管理',
          }
        },
        {
          path: '/sysMenu',
          name: 'sysMenu',
          component: () => import('@/components/sysmenu/sysMenu.vue'),
          meta: {
            title: '菜单资源管理',
          }
        },
        {
          path: '/sysApi',
          name: 'sysApi',
          component: () => import('@/components/sysapi/sysApi.vue'),
          meta: {
            title: '菜单接口',
          }
        },
        {
          path: '/sqlFormat',
          name: 'sqlFormat',
          component: () => import('@/components/tools/sqlformat/sqlFormat.vue'),
          meta: {
            title: 'sql格式化',
          }
        },
        {
          path: '/activiti',
          name: 'activiti',
          component: () => import('@/components/activiti/activiti.vue'),
          meta: {
            title: '工作流管理',
          }
        },
        {
          path: '/quartz',
          name: 'quartz',
          component: () => import('@/components/quartz/quartz.vue'),
          meta: {
            title: '定时任务管理',
          }
        },
        {
          path: '/leave',
          name: 'leave',
          component: () => import('@/components/bizleave/bizleave.vue'),
          meta: {
            title: '请假申请',
          }
        },
        {
          path: '/leaderapply',
          name: 'leaderapply',
          component: () => import('@/components/bizleave/leaderapply.vue'),
          meta: {
            title: '组长审批',
          }
        },
        {
          path: '/managerapply',
          name: 'managerapply',
          component: () => import('@/components/bizleave/managerapply.vue'),
          meta: {
            title: '经理审批',
          }
        },
        {
          path: '/activitihis',
          name: 'activitihis',
          component: () => import('@/components/activiti/activitiHis.vue'),
          meta: {
            title: '审批记录',
          }
        },
        {
          path: '/basicHoliday',
          name: 'basicHoliday',
          component: () => import('@/components/basicholiday/basicHoliday.vue'),
          meta: {
            title: '节假日管理',
          }
        },
        {
          path: '/report',
          name: 'report',
          component: () => import('@/components/report/report.vue'),
          meta: {
            title: '报表设计',
          }
        },
        {
          path: '/reportTpl',
          name: 'reportTpl',
          component: () => import('@/components/report/reporttpl.vue'),
          meta: {
            title: '报表模板管理',
          }
        },
        {
          path: '/reportDatasource',
          name: 'reportDatasource',
          component: () => import('@/components/report/reportdatasource.vue'),
          meta: {
            title: '报表数据库管理',
          }
        },
        {
          path: '/generateReport',
          name: 'generateReport',
          component: () => import('@/components/report/generateReport.vue'),
          meta: {
            title: '生成报表',
          }
        },
        {
          path: '/echarts',
          name: 'echarts',
          component: () => import('@/components/report/charts.vue'),
          meta: {
            title: '图表',
          }
        },
        {
          path: '/chartManage',
          name: 'chartManage',
          component: () => import('@/components/report/chartManage.vue'),
          meta: {
            title: '图表设计',
          }
        },
        {
          path: '/htmlview',
          name: 'htmlview',
          component: () => import('@/components/report/preview.vue'),
          meta: {
            title: '图表设计',
          }
        },
      ]
    },
    {
      name: '404',
      path: '/404',
      component: () => import('@/components/homepage/404.vue'),
      meta: { title: '路由不存在' }
    },
    {
      name: '403',
      path: '/403',
      component: () => import('@/components/homepage/403.vue'),
      meta: { title: '资源不可访问' }
    },
    {
      path: '*',
      redirect: '/404'
    }
  ],
  mode: 'history'
})
/**
 * 全局路由守卫
 */
const rightPathList = ['/login', '/404', '/403'];//直接可以进入的页面
router.beforeEach((to, from, next) => {
  // debugger
  console.log('跳转到:', to.fullPath);
  var token = sessionStorage.getItem('token');
  // if (!token && to.path != '/login') {//登陆认证校验,没登录则跳转/login
  //   next({ path: '/login' })
  // }
  // else {//权限认证
    if (rightPathList.includes(to.path)) {
      next();
    }
    else {
      next();
    }
    // else {
    //   next('403');
    // }

  // }
})
/**
 * 请求拦截器,添加请求头token
 */
axios.interceptors.request.use(
  config => {
    console.log('>>>请求url:',config.url);
    var headers = config.headers;
    if (sessionStorage.getItem("token")) {
      headers.token = sessionStorage.getItem("token");
    }
    return config;
  },
  error => {
    console.log('>>>请求异常:',error.message);
    return Promise.reject(error);
  });
//接口请求超时设置
axios.defaults.timeout=30000;//毫秒
/**
 * 应答拦截器,添加请求头token
 */
axios.interceptors.response.use(function (response) {
  // Do something with response data
  console.log('<<<请求成功');
  return response;
}, error=> {
  // Do something with response error
  console.log('<<<异常信息:',error.message);
  return Promise.reject(error);
});


//获取当前路由是否有权限访问
function hasPermit(to) {
  var hasPermit = false;
  if (to.meta && to.meta.role) {
    var ruleList = getRuleList();
    hasPermit = ruleList.some(rule => {
      return to.meta.role.includes(rule);
    });
  }
  return hasPermit;

}
//获取登陆的角色集合
function getRuleList() {
  var ruleList = []; //角色数组
  var strRule = sessionStorage.getItem("position");
  if (strRule.indexOf("|") != -1) {
    ruleList = strRule.split("|");
  } else {
    ruleList.push(strRule);
  }
  return ruleList;
}

export default router
