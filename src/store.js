import Vue from 'vue'
import Vuex from 'vuex'
import commonUtil from './components/common/common'
import commonConstants from './components/common/constants'
Vue.use(Vuex)

const actions = {
    //获取表格数据
    searchtablelist({commit},obj)
    {
        commit('searchtablelist',obj)
    },
    //弹出对话框
    showModal({commit},obj){
        commit('showModal',obj)
    },
    //新增页面打开
    showInsert({commit},obj){
        commit('showInsert',obj)
    },
    //详情页面打开
    showDetail({commit},obj){
        commit('showDetail',obj)
    },
    //获取详情回调函数
    showDetailCallback({commit},obj){
        commit('showDetailCallback',obj)
    },
    //编辑页面打开
    showEdit({commit},obj){
        commit('showEdit',obj)
    },
    showEditCallback({commit},obj){
        commit('showEditCallback',obj)
    },
    //提交操作
    save({commit},obj){
        commit('save',obj)
    },
    //提交操作回调函数
    saveCallback({commit},obj){
        commit('saveCallback',obj)
    },
    //关闭对话框
    closeModal({commit},obj){
        commit('closeModal',obj)
    },
    //删除单条数据
    delete({commit},obj){
        commit('delete',obj)
    },
    //删除回调函数
    deleteCallback({commit},obj){
        commit('deleteCallback',obj)
    },
    //选中表格数据事件
    selectChange({commit},obj){
        commit('selectChange',obj)
    },
    //删除多条单条数据
    deleteBatch({commit},obj){
        commit('deleteBatch',obj)
    },
    //重置查询
    reset({commit},obj){
        commit('reset',obj)
    },
}

const mutations = {
    searchtablelist (state,obj) {
        var param = Object.assign({}, obj.queryData, obj.tablePage);
        var object = {
            url:obj.requestUrl.listApi,
            params:param,
            tableData:obj.tableData,
            tablePage:obj.tablePage
        }
        commonUtil.getTableList(object);
    },
    showModal (state,obj) {
        obj.pageData.modalConfig.show = true;
       
        if(obj.pageData.modalConfig.type == commonConstants.modalType.insert)
        {
            obj.pageData.modalConfig.title = commonConstants.modalTitle.insert
            obj.pageData.modalConfig.submitUrl = obj.pageData.requestUrl.insertApi;
        }else if(obj.pageData.modalConfig.type == commonConstants.modalType.update)
        {
            obj.pageData.modalConfig.title = commonConstants.modalTitle.update
            obj.pageData.modalConfig.submitUrl = obj.pageData.requestUrl.updateApi;
        }else if(obj.pageData.modalConfig.type == commonConstants.modalType.detail)
        {
            obj.pageData.modalConfig.title = commonConstants.modalTitle.detail
            obj.pageData.modalConfig.submitUrl ="";
        }
        if(obj.init)
        {
            obj.init();
        }
    },
    showInsert(state,obj){
        obj.pageData.modalConfig.submitUrl=obj.pageData.requestUrl.updateApi;
        obj.pageData.modalConfig.type=commonConstants.modalType.insert;
        obj.pageData.removeEmpty = obj.removeEmpty;
        obj.pageData.that.showModal(obj);
    },
    showDetail (state,obj) {
        obj.pageData.modalConfig.submitUrl="";
        obj.pageData.modalConfig.type=commonConstants.modalType.detail;
        obj.pageData.removeEmpty = false;
        var object = {
            url:obj.pageData.requestUrl.getDetailApi,
            params:obj.param,
            removeEmpty:false,
            callback:obj.callback?obj.callback:obj.pageData.that.showDetailCallback,
            that:obj.pageData.that,
            init:obj.init
        }
        commonUtil.doAjaxGet(object);
    },
    showDetailCallback(state,response) {
        if(response.statusCode == "200")
        {
            response.that.showModal({pageData:response.that.pageData,init:response.init});
            var responseData = response.responseData;
            // response.that.pageData.modalData = responseData;
            commonUtil.coperyProperties(response.that.pageData.modalData,responseData);
        }
    },
    showEdit (state,obj) {
        obj.pageData.modalConfig.submitUrl=obj.pageData.requestUrl.updateApi;
        obj.pageData.modalConfig.type=commonConstants.modalType.update;
        obj.pageData.modalConfig.removeEmpty = obj.removeEmpty;
        var object = {
            url:obj.pageData.requestUrl.getDetailApi,
            params:obj.param,
            removeEmpty:false,
            callback:obj.callback?obj.callback:obj.pageData.that.showEditCallback,
            that:obj.pageData.that,
            init:obj.init
        }
        commonUtil.doAjaxGet(object);
    },
    showEditCallback(state,response) {
        if(response.statusCode == "200")
        {
            response.that.showModal({pageData:response.that.pageData,init:response.init});
            var responseData = response.responseData;
            // response.that.pageData.modalData = responseData;
            commonUtil.coperyProperties(response.that.pageData.modalData,responseData);
        }
    },
    save (state,obj) {
        obj.pageData.that.$refs[obj.pageData.modalConfig.modalRef].$refs['modalFormRef'].validate((valid) => {
            if (valid) {
                //将删除标志 创建人更新人以及时间移除
                delete obj.pageData.modalData['delFlag']
                delete obj.pageData.modalData['creator']
                delete obj.pageData.modalData['createTime']
                delete obj.pageData.modalData['updater']
                delete obj.pageData.modalData['updateTime']
                var object = {
                    url:obj.pageData.modalConfig.submitUrl,
                    params:obj.pageData.modalData,
                    removeEmpty:obj.pageData.modalConfig.removeEmpty,
                    callback:obj.callback?obj.callback:obj.pageData.that.saveCallback,
                    that:obj.pageData.that
                }
                commonUtil.doAjaxPost(object);
            }else{
                return false;
            }
        });
    },
    saveCallback (state,obj) {
        if(obj.statusCode == "200")
        {
            obj.that.closeModal(obj.that.pageData)
            obj.that.searchtablelist(obj.that.pageData);
        }
        
    },
    closeModal (state,obj) {
        obj.that.$refs[obj.modalConfig.modalRef].$refs['modalFormRef'].resetFields();
        obj.modalConfig.show = false;
        commonUtil.clearObj(obj.modalData)
    },
    delete (state,obj) {
        let params = {
            url:obj.pageData.requestUrl.deleteOneApi,
            messageContent:commonUtil.getMessageFromList("confirm.delete",null),
            callback:obj.callback?obj.callback:obj.pageData.that.deleteCallback,
            requestParam:obj.param,
            type:"get",
            that:obj.pageData.that
        }
        //弹出删除确认框
        commonUtil.showConfirm(params)
    },
    deleteCallback (state,obj) {
        obj.that.searchtablelist(obj.that.pageData);
    },
    selectChange(state,obj) {
        obj.pageData.selectList = obj.rows;
    },
    deleteBatch(state,obj) {
        const length = obj.pageData.selectList.length;
        if(length == 0)
        {
            commonUtil.showMessage({message:commonUtil.getMessageFromList("error.batchdelete.empty",null),type: commonConstants.messageType.error});
        }else{
            let ids = new Array();
            for (let i = 0; i < length; i++) {
                ids.push(obj.pageData.selectList[i].id);
            }
            let params = {
                url:obj.pageData.requestUrl.deleteBatchApi,
                messageContent:commonUtil.getMessageFromList("confirm.delete",null),
                callback:obj.callback?obj.callback:obj.pageData.that.deleteCallback,
                requestParam:ids,
                type:"post",
                that:obj.pageData.that
            }
            //弹出删除确认框
            commonUtil.showConfirm(params)
        }
    },
    reset(state,obj) {
        commonUtil.clearObj(obj.queryData);
        obj.that.searchtablelist(obj);
    }
    
}
export default new Vuex.Store({
    actions,
    mutations,
})