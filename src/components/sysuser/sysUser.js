import { mapState, mapGetters, mapActions, mapMutations } from 'vuex'
export default {
  name:'sysUser',
  data() {
    return{
      pageData:{
        that:this,
        //请求的url start
        requestUrl:{
          listApi:"/api/sysUser/getTableList",//获取表格数据api
          insertApi:"/api/sysUser/insert",//新增用api
          updateApi:"/api/sysUser/update",//更新用api
          getDetailApi:"/api/sysUser//getDetail",//获取详情用api
          deleteOneApi:"/api/sysUser/delete",//单条删除api
          deleteBatchApi:"/api/sysUser/deletebatch",//批量删除api
          getRolesApi:"/api/sysRole/getRoles",//获取角色
        },
        //请求的url end
        //查询表单内容 start
        searchForm:[
          {type:'Input',label:'用户登录名',prop:'userName'},
          {type:'Input',label:'用户真实姓名',prop:'userRealName'},
          {type:'Input',label:'用户邮箱',prop:'userEmail'},
          {type:'Input',label:'座机',prop:'userPhone'},
          {type:'Input',label:'手机',prop:'userMobile'},
          {type:'Select',label:'是否锁定',prop:'userLocked',options:this.selectUtil.yesNo,props:this.commonConstants.props,},
          {type:'Select',label:'是否管理员',prop:'isAdmin',options:this.selectUtil.yesNo,props:this.commonConstants.props,},
        ],
        //查询表单内容 end
        //查询条件 start
        queryData:{
          userName:"",//用户登录名
          userRealName:"",//用户真实姓名
          password:"",//用户密码
          userEmail:"",//用户邮箱，唯一
          userPhone:"",//座机
          userMobile:"",//手机，唯一
          userHead:"",//用户头像
          userLocked:"",//用户是否锁定 1是 2否
        },
        //查询条件 end
        //查询表单按钮start
        searchHandle:[
          {label:'查询',type:'primary',handle:()=>this.searchtablelist(this.pageData),auth:'sysUser_search'},
          {label:'重置',type:'warning',handle:()=>this.reset(this.pageData),auth:'sysUser_search'}
        ],
        //查询表单按钮end
        //表格数据start
        tableData:[],
        //表格数据end
        //表格工具栏按钮 start
        tableHandles:[
          {label:'新增',type:'primary',handle:()=>this.showInsert({pageData:this.pageData,removeEmpty:false,init:this.getRoles}),auth:'sysUser_insert'},
          {label:'批量删除',type:'danger',handle:()=>this.deleteBatch({pageData:this.pageData}),auth:'sysUser_batchdelete'}
        ],
        //表格工具栏按钮 end
        selectList:[],//表格选中的数据
        //表格分页信息start
        tablePage:{
          currentPage: 1,
          pageSize:10,
          pageTotal: 0,
          pageSizeRange:[5, 10, 20, 50]
        },
        //表格分页信息end
        //表格列表头start
        tableCols:[
          {label:'头像',prop:'userHead',align:'center',type:'image',overflow:true,popover:true},
          {label:'用户登录名',prop:'userName',align:'center'},
          {label:'用户姓名',prop:'userRealName',align:'center'},
          {label:'用户邮箱',prop:'userEmail',align:'center'},
          {label:'座机',prop:'userPhone',align:'center'},
          {label:'手机',prop:'userMobile',align:'center'},
          {label:'是否管理员',prop:'isAdmin',align:'center',formatter:this.commonUtil.getCodeName},
          {label:'是否锁定',prop:'userLocked',align:'center',formatter:this.commonUtil.getCodeName},
          {label:'操作',prop:'operation',align:'center',type:'button',btnList:[
            {label:'查看',type:'text',auth:'sysUser_getdetail',handle:(that,row)=>this.showDetail({pageData:this.pageData,param:{id:row.id},callback:this.getDetailBack})},
            {label:'编辑',type:'text',auth:'sysUser_update',handle:(that,row)=>this.showEdit({pageData:this.pageData,param:{id:row.id},removeEmpty:false,callback:this.getDetailBack})},
            {label:'删除',type:'text',auth:'sysUser_delete',handle:(that,row)=>this.delete({pageData:this.pageData,param:{id:row.id}})},
          ]}
        ],
        //表格列表头end
        //modal配置 start
        modalConfig:{ 
          title: "新增", //弹窗标题,值为:新增，查看，编辑
          show: false, //弹框显示
          formEditDisabled:false,//编辑弹窗是否可编辑
          width:'700px',//弹出框宽度
          type:'1',//类型 1新增 2编辑 3详情
          removeEmpty:false,//空字符串是否作为参数传递到后台 false:空字符串将传递到后台 true:空字符串将不会传递到后台，后台对应的值为null
          submitUrl:'',//提交按钮对应的url
          disabled:false,//按钮是否可用
          modalRef:"modalRef",//modal标识
        },
        //modal配置 end
        //modal表单 start
        modalForm:[
          {type:'Upload',label:'用户头像',tips:'上传头像',width:'500px',prop:'fileList',multiple:false,rules:{required:false},accept:".jpg,.jpeg,.png,.gif,.bmp,.JPG,.JPEG,.PBG,.GIF,.BMP"},
          {type:'Input',label:'用户登录名',prop:'userName',rules:{required:true,maxLength:40}},
          {type:'Input',label:'用户姓名',prop:'userRealName',rules:{required:true,maxLength:40}},
          {type:'Input',label:'用户邮箱',prop:'userEmail',rules:{required:true,type:'email',maxLength:50}},
          {type:'Input',label:'座机',prop:'userPhone',rules:{required:true,type:'phone',maxLength:15}},
          {type:'Input',label:'手机',prop:'userMobile',rules:{required:true,type:'mobile',maxLength:50}},
          {type:'Select',label:'是否锁定',prop:'userLocked',rules:{required:true},options:this.selectUtil.yesNo},
          {type:'Select',label:'是否管理员',prop:'isAdmin',rules:{required:true},options:this.selectUtil.yesNo,change:()=>this.changeIsAdmin()},
          {type:'Select',label:'用户角色',prop:'roles',rules:{required:true},props:{label:'roleName',value:'id'},multiple:true,show:true},
        ],
        //modal表单 end
        //modal 数据 start
        modalData : {//modal页面数据
          userName:"",//用户登录名 
          userRealName:"",//用户真实姓名 
          userEmail:"",//用户邮箱，唯一 
          userPhone:"",//座机 
          userMobile:"",//手机，唯一 
          userHeadName:"",//用户头像名称
          userHead:"",//用户头像 
          userLocked:"",//用户是否锁定 1是 2否 
          fileList:[],//文件列表
          roles:[],//用户角色
          isAdmin:"",//是否管理员
        },
        //modal 数据 end
        //modal 按钮 start
        modalHandles:[
          {label:'取消',type:'default',handle:()=>this.closeModal(this.pageData)},
          {label:'提交',type:'primary',handle:()=>this.save({pageData:this.pageData})}
        ],
        //modal 按钮 end
        //其他参数 start
        roles:[],//角色
        //其他参数 end
      }
    }
  },
  mounted(){
    this.searchtablelist(this.pageData);
  },
  methods:{
    ...mapActions([
      'searchtablelist','showModal','save','closeModal','showDetail','delete','saveCallback','showDetailCallback',
      'showEdit','showEditCallback','showInsert','deleteCallback','selectChange','deleteBatch','reset'
    ]),
    //获取详情回调函数
    getDetailBack(response){
      if(response.statusCode == "200")
      {
        this.showModal({pageData:this.pageData,init:this.getRoles})
        var responseData = response.responseData;
        this.commonUtil.coperyProperties(this.pageData.modalData,responseData);
        // this.pageData.modalData = responseData;
        let file = {
          name:responseData.userHeadName,
          url:responseData.userHead
        }
        this.pageData.modalData.fileList.push(file)
        if(this.pageData.modalData.isAdmin == "1")
        {
          this.pageData.modalForm[8].show=false;
          this.pageData.modalData.roles = [];
          this.pageData.modalForm[8].rules={};
        }else{
          this.pageData.modalForm[8].show=true;
          this.pageData.modalForm[8].rules={required:true};
        }
      }
    },
    //获取角色
    getRoles(){
      var self = this;
      let param = {
        url:this.pageData.requestUrl.getRolesApi,
        params:{},
        removeEmpty:false,
        callback:this.getRolesCallback
      }
      this.commonUtil.doAjaxPost(param)
    },
    getRolesCallback(data){
      this.$refs['modalRef'].modalForm[8].options = data.responseData;
      this.$refs['modalRef'].$forceUpdate();//在methods中需强制更新，mounted中不需要
    },
    changeIsAdmin(){
      if(this.pageData.modalData.isAdmin == "1")
      {
        this.pageData.modalForm[8].show=false;
        this.pageData.modalData.roles = [];
        this.pageData.modalForm[8].rules={};
      }else{
        this.pageData.modalForm[8].show=true;
        this.pageData.modalData.roles = [];
        this.pageData.modalForm[8].rules={required:true};
      }
    },
  }
};