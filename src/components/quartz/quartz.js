import { mapState, mapGetters, mapActions, mapMutations } from 'vuex'
export default {
  name:'bizLeave',
  data() {
    return{
      pageData:{
        that:this,
        cronPopover:false,
        cron:"",
        //请求的url start
        requestUrl:{
          listApi:"/api/quartz/getTableList",//获取表格数据api
          insertApi:"/api/quartz/insert",//新增用api
          updateApi:"/api/quartz/update",//更新用api
          pauseApi:"/api/quartz/pause",//暂停任务api
          resumeApi:"/api/quartz/resume",//恢复任务api
          deleteOneApi:"/api/quartz/delete",//单条删除api
        },
        //请求的url end
        //查询表单内容 start
        searchForm:[
        //   {type:'Input',label:'日期',prop:'date'},
        ],
        //查询表单内容 end
        //查询条件 start
        queryData:{
          date:"",//日期
        },
        //查询条件 end
        //查询表单按钮start
        searchHandle:[
          {label:'查询',type:'primary',handle:()=>this.searchtablelist(this.pageData),auth:'sysUser_search'},
          {label:'重置',type:'warning',handle:()=>this.reset(this.pageData),auth:'sysUser_search'}
        ],
        //查询表单按钮end
        //表格数据start
        tableData:[],
        //表格数据end
        //表格工具栏按钮 start
        tableHandles:[
          {label:'新增',type:'primary',handle:()=>this.showInsert({pageData:this.pageData,removeEmpty:false}),auth:'sysUser_insert'},
        ],
        //表格工具栏按钮 end
        selectList:[],//表格选中的数据
        //表格分页信息start
        tablePage:{
          currentPage: 1,
          pageSize:10,
          pageTotal: 0,
          pageSizeRange:[5, 10, 20, 50]
        },
        //表格分页信息end
        //表格列表头start
        tableCols:[
          {label:'任务名称',prop:'jobName',align:'center'},
          {label:'任务分组',prop:'jobGroupName',align:'center'},
          {label:'类名',prop:'jobClassName',align:'center'},
          {label:'触发组',prop:'triggerGroupName',align:'center'},
          {label:'触发器',prop:'triggerName',align:'center'},
          {label:'上次执行时间',prop:'prevFireTime',align:'center'},
          {label:'下次执行时间',prop:'nextFireTime',align:'center'},
          {label:'时间表达式',prop:'cronExpression',align:'center'},
          {label:'状态',prop:'triggerState',align:'center'},
          {label:'任务描述',prop:'jobDescription',align:'center'},
          {label:'操作',prop:'operation',align:'center',type:'button',btnList:[
            {label:'编辑',type:'text',auth:'sysUser_update',handle:(that,row)=>this.edit(row)},
            {label:'暂停',type:'text',auth:'sysUser_getdetail',handle:(that,row)=>this.pause(row),disabled:this.pauseDisabled},
            {label:'恢复',type:'text',auth:'sysUser_getdetail',handle:(that,row)=>this.resume(row),disabled:this.resumeDisabled},
            {label:'删除',type:'text',auth:'sysUser_delete',handle:(that,row)=>this.deleteJob(row)},
          ]}
        ],
        //表格列表头end
        //modal配置 start
        modalConfig:{ 
          title: "新增", //弹窗标题,值为:新增，查看，编辑
          show: false, //弹框显示
          formEditDisabled:false,//编辑弹窗是否可编辑
          width:'700px',//弹出框宽度
          type:'1',//类型 1新增 2编辑 3详情
          removeEmpty:false,//空字符串是否作为参数传递到后台 false:空字符串将传递到后台 true:空字符串将不会传递到后台，后台对应的值为null
          disabled:false,//按钮是否可用
          modalRef:"modalRef",//modal标识
        },
        //modal配置 end
        //modal表单 start
        modalForm:[
          {type:'Input',label:'触发器名称',prop:'triggerName',rules:{required:true},disabled:this.modalDisabled},
          {type:'Input',label:'任务名称',prop:'jobName',rules:{required:true},disabled:this.modalDisabled},
          {type:'Input',label:'类名',prop:'jobClassName',rules:{required:true},disabled:this.modalDisabled},
          {type:'Input',label:'时间表达式',prop:'cronExpression',rules:{required:true},cron:false,changeCron:this.changeCron,focus:this.focusCronInput},
        ],
        //modal表单 end
        //modal 数据 start
        modalData : {//modal页面数据
            triggerName:"",
            jobName:"",
            jobClassName:"",
            cronExpression:""
        },
        //modal 数据 end
        //modal 按钮 start
        modalHandles:[
          {label:'取消',type:'default',handle:()=>this.closeModal(this.pageData)},
          {label:'提交',type:'primary',handle:()=>this.save({pageData:this.pageData})}
        ],
        //modal 按钮 end
      }
    }
  },
  mounted(){
    this.searchtablelist(this.pageData);
  },
  methods:{
    ...mapActions([
      'searchtablelist','showModal','save','closeModal','showDetail','delete','saveCallback','showDetailCallback',
      'showEdit','showEditCallback','showInsert','deleteCallback','selectChange','deleteBatch','reset'
    ]),
    edit(row){
        this.pageData.modalConfig.show = true;
        this.pageData.modalConfig.submitUrl=this.pageData.requestUrl.updateApi;
        this.pageData.modalConfig.type=this.commonConstants.modalType.update;
        this.pageData.modalConfig.removeEmpty = false;
        this.pageData.modalData.triggerName = row.triggerName;
        this.pageData.modalData.jobName = row.jobName;
        this.pageData.modalData.jobClassName = row.jobClassName;
        this.pageData.modalData.cronExpression = row.cronExpression;
    },
    pause(row){//暂停任务
        let param = {
            url:this.pageData.requestUrl.pauseApi,
            params:{jobName:row.jobName},
            removeEmpty:false,
            callback:this.pauseCallback
          }
          this.commonUtil.doAjaxPost(param);
    },
    pauseCallback(response){
        this.searchtablelist(this.pageData);
    },
    resume(row){//恢复任务
        let param = {
            url:this.pageData.requestUrl.resumeApi,
            params:{jobName:row.jobName},
            removeEmpty:false,
            callback:this.pauseCallback
          }
          this.commonUtil.doAjaxPost(param);
    },
    pauseDisabled(row){
        if(row.triggerState == "PAUSED")
        {
            return true;
        }else{
            return false;
        }
    },
    resumeDisabled(row){
        if(row.triggerState != "PAUSED")
        {
            return true;
        }else{
            return false;
        }
        
    },
    modalDisabled(){
        if(this.pageData.modalConfig.type==this.commonConstants.modalType.update)
        {
            return true;
        }else{
            return false;
        }
    },
    deleteJob(row){
        //删除任务
        let param = {
            url:this.pageData.requestUrl.deleteOneApi,
            params:{jobName:row.jobName,triggerName:row.triggerName},
            removeEmpty:false,
            callback:this.pauseCallback
          }
          this.commonUtil.doAjaxPost(param);
    },
    changeCron(val){
      this.pageData.modalData.cronExpression = val;
    },
    focusCronInput(){
      this.pageData.modalForm[3].cron = true;
    }
  }
};