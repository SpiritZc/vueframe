import { mapState, mapGetters, mapActions, mapMutations } from 'vuex'
export default {
  name:'leaderapply',
  data() {
    return{
      pageData:{
        that:this,
        //请求的url start
        requestUrl:{
          approvalApi:"/api/bizLeave/leaderApproval",//组长审批
          detailApi:"/api/bizLeave/getDetailByKey",//获取详情api
        },
        //请求的url end
        //查询表单内容 start
        searchForm:[
          {type:'Date',label:'请假开始时间',prop:'leaveTimeStart'},
          {type:'Date',label:'请假结束时间',prop:'leaveTimeEnd'},
          {type:'Input',label:'请假天数',prop:'leaveDays'},
          {type:'Input',label:'请假原因',prop:'leaveReason'},
          {type:'Input',label:'审批意见',prop:'comment',rules:{required:true}},
          {type:'Radio',label:'审批结果',prop:'status',radios:[{'label':'同意','value':'1'},{'label':'不同意','value':'2'}],rules:{required:true}},
        ],
        //查询表单内容 end
        //查询条件 start
        queryData:{
          leaveTimeStart:"",//请假开始时间
          leaveTimeEnd:"",//请假结束时间
          leaveDays:"",//请假天数
          leaveReason:"",//请假原因
          status:"1",//审批意见
          comment:"",//审批意见
        },
        //查询条件 end
        //查询表单按钮start
        searchHandle:[
          {label:'提交',type:'primary',handle:()=>this.approval(),auth:'act_approval'},
        ],
        
      }
    }
  },
  mounted(){
    this.getLeaveDetail();
  },
  methods:{
    ...mapActions([
      // 'searchtablelist','showModal','save','closeModal','showDetail','delete','saveCallback','showDetailCallback',
      // 'showEdit','showEditCallback','showInsert','deleteCallback','selectChange','deleteBatch','reset'
    ]),
    //获取详情
    getLeaveDetail(){
      var businessKey = this.$route.query['businessKey'];
      let param = {
        url:this.pageData.requestUrl.detailApi,
        params:{leaveCode:businessKey},
        removeEmpty:false,
        callback:this.getLeaveDetailCallback
      }
      this.commonUtil.doAjaxPost(param);
    },
    getLeaveDetailCallback(response){
      var responseData = response.responseData;
      console.log(responseData)
      this.commonUtil.coperyProperties(this.pageData.queryData,responseData);
    },
    //审批
    approval(){
      this.$refs['searchForm'].$refs['searchFormRef'].validate((valid) => {
        if (valid) {
          var businessKey = this.$route.query['businessKey'];
          let param = {
            url:this.pageData.requestUrl.approvalApi,
            params:{taskId:this.$route.query['taskId'],status:this.pageData.queryData.status,comment:this.pageData.queryData.comment,businessKey:businessKey},
            removeEmpty:false,
            callback:this.approvalCallback
          }
          this.commonUtil.doAjaxPost(param);
        }else{
            return false;
        }
    });
    },
    approvalCallback(response){
      if(response.statusCode=="200")
      {//审批成功关闭审批页面，跳转到首页
        this.commonUtil.closeTag("leaderapply",this);
      }
    }
    
  }
};