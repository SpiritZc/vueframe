import { mapState, mapGetters, mapActions, mapMutations } from 'vuex'
export default {
  name:'bizLeave',
  data() {
    return{
      pageData:{
        that:this,
        //请求的url start
        requestUrl:{
          listApi:"/api/basicHoliday/getTableList",//获取表格数据api
          insertApi:"/api/basicHoliday/insert",//新增用api
          updateApi:"/api/basicHoliday/update",//更新用api
          getDetailApi:"/api/basicHoliday//getDetail",//获取详情用api
          deleteOneApi:"/api/basicHoliday/delete",//单条删除api
          deleteBatchApi:"/api/basicHoliday/deletebatch",//批量删除api
          getRolesApi:"/api/sysRole/getRoles",//获取角色
        },
        //请求的url end
        //查询表单内容 start
        searchForm:[
          {type:'Input',label:'日期',prop:'date'},
        ],
        //查询表单内容 end
        //查询条件 start
        queryData:{
          date:"",//日期
        },
        //查询条件 end
        //查询表单按钮start
        searchHandle:[
          {label:'查询',type:'primary',handle:()=>this.searchtablelist(this.pageData),auth:'sysUser_search'},
          {label:'重置',type:'warning',handle:()=>this.reset(this.pageData),auth:'sysUser_search'}
        ],
        //查询表单按钮end
        //表格数据start
        tableData:[],
        //表格数据end
        //表格工具栏按钮 start
        tableHandles:[
          {label:'新增',type:'primary',handle:()=>this.showInsert({pageData:this.pageData,removeEmpty:false}),auth:'sysUser_insert'},
          {label:'批量删除',type:'danger',handle:()=>this.deleteBatch({pageData:this.pageData}),auth:'sysUser_batchdelete'}
        ],
        //表格工具栏按钮 end
        selectList:[],//表格选中的数据
        //表格分页信息start
        tablePage:{
          currentPage: 1,
          pageSize:10,
          pageTotal: 0,
          pageSizeRange:[5, 10, 20, 50]
        },
        //表格分页信息end
        //表格列表头start
        tableCols:[
          {label:'日期',prop:'date',align:'center'},
          {label:'星期几',prop:'weekday',align:'center',formatter:this.commonUtil.getCodeName},
          {label:'假期类型',prop:'holidayType',align:'center',formatter:this.commonUtil.getCodeName},
          {label:'操作',prop:'operation',align:'center',type:'button',btnList:[
            {label:'查看',type:'text',auth:'sysUser_getdetail',handle:(that,row)=>this.showDetail({pageData:this.pageData,param:{id:row.id},callback:this.getDetailBack})},
            {label:'编辑',type:'text',auth:'sysUser_update',handle:(that,row)=>this.showEdit({pageData:this.pageData,param:{id:row.id},removeEmpty:false,callback:this.getDetailBack})},
            {label:'删除',type:'text',auth:'sysUser_delete',handle:(that,row)=>this.delete({pageData:this.pageData,param:{id:row.id}})},
          ]}
        ],
        //表格列表头end
        //modal配置 start
        modalConfig:{ 
          title: "新增", //弹窗标题,值为:新增，查看，编辑
          show: false, //弹框显示
          formEditDisabled:false,//编辑弹窗是否可编辑
          width:'700px',//弹出框宽度
          type:'1',//类型 1新增 2编辑 3详情
          removeEmpty:false,//空字符串是否作为参数传递到后台 false:空字符串将传递到后台 true:空字符串将不会传递到后台，后台对应的值为null
          disabled:false,//按钮是否可用
          modalRef:"modalRef",//modal标识
        },
        //modal配置 end
        //modal表单 start
        modalForm:[
          {type:'Date',label:'日期',prop:'date',rules:{required:true}},
          {type:'Select',label:'星期几',prop:'weekday',rules:{required:true},options:this.selectUtil.weekday},
          {type:'Select',label:'假期类型',prop:'holidayType',rules:{required:true},options:this.selectUtil.holidayType},
        ],
        //modal表单 end
        //modal 数据 start
        modalData : {//modal页面数据
            date:"",//时间
            weekday:"",//星期几
            holidayType:"",//假期类型
        },
        //modal 数据 end
        //modal 按钮 start
        modalHandles:[
          {label:'取消',type:'default',handle:()=>this.closeModal(this.pageData)},
          {label:'提交',type:'primary',handle:()=>this.save({pageData:this.pageData})}
        ],
        //modal 按钮 end
        //其他参数 start
        roles:[],//角色
        //其他参数 end
      }
    }
  },
  mounted(){
    this.searchtablelist(this.pageData);
  },
  methods:{
    ...mapActions([
      'searchtablelist','showModal','save','closeModal','showDetail','delete','saveCallback','showDetailCallback',
      'showEdit','showEditCallback','showInsert','deleteCallback','selectChange','deleteBatch','reset'
    ]),
  }
};