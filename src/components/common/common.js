import Vue from 'vue'
import { Message,MessageBox } from 'element-ui'
import MSG_LIST from './message'
import Axios from 'axios'
import moment from 'moment'
import commonConstants from './constants'
import router from '../../router'
const commonUtil = {
 
}

//显示信息
// showClose 是否显示关闭按钮 boolean
//message 消息内容
//type 消息类型 success/warning/info/error
//duration 显示时间, 毫秒。设为 0 则不会自动关闭
commonUtil.showMessage = function(obj)
{
    Message({
        showClose: (obj.showClose == undefined)?true:obj.showClose,
        message: obj.message,
        type: (obj.type == undefined)?'success':obj.type,
        duration: (obj.duration == undefined)?2000:obj.duration
    })
}

//获取错误消息
commonUtil.getMessageFromList = function(msgid,msgParams)
{
    let msg = MSG_LIST[msgid];
    if ($.trim(msg) == "") {
        msg = msgid + "[该消息不存在。]";
        return msg;
    }
    if (msgParams != null) { 
        for (var i=0; i<msgParams.length; i++) {
            msg = msg.replace("{" + i + "}", msgParams[i]);
        }  
    }
    return msg;
}


//获取表格数据字典值
commonUtil.getTableCodeName = function(row, column)
{
    var type = column.property;
    var value = row[column.property];
    if(type)
    {
        return commonUtil.getDictionaryValueName(type,value)
    }else{
        return "-"
    }
}
//获取表格数据字典值
commonUtil.getCodeName = function(name,value)
{
    return commonUtil.getDictionaryValueName(name,value)
}
//表格日期格式化
commonUtil.formatTableDate = function(row, column)
{
    var date = row[column.property];
    if (date == undefined) {
      return "";
    }
    return commonUtil.formatDate(date);
}
//日期格式化
commonUtil.formatDate = function(val)
{
   return moment(val).format("YYYY-MM-DD HH:mm:ss");
}

//获取表格数据
//url:后台请求地址
//params:后台请求参数
//tableData:表格数据
//tablePage:表格分页参数
// commonUtil.getTableList = function(url,params,tableData,tablePage)
commonUtil.getTableList = function(obj)
{
    commonUtil.removeEmpty(obj.params);
    console.log(obj)
    obj.params.limit = obj.tablePage.pageSize;//每页显示的条数
    obj.params.current = obj.tablePage.currentPage;//当前页数
    Axios.post(obj.url,obj.params,{headers: {
    'Authorization':sessionStorage.getItem("token")}})
    .then(response => {
        if(response.status == 200)
        {
            //请求成功
            var result = response.data;
            var responseData = result.responseData;
            if (result.statusCode == "200")
            {
                if(result.message)
                {
                    commonUtil.showMessage({message:result.message,type: result.msgLevel})
                }
                obj.tableData.splice(0)
                if(responseData.data.length > 0)
                {
                    for (let index = 0; index < responseData.data.length; index++) {
                        obj.tableData.push(responseData.data[index])
                    }
                }
                obj.tablePage.pageTotal = responseData.total;
            }else{
                if(result.statusCode == "50004")
                {
                    sessionStorage.removeItem('token');
                    router.replace('/login');
                }
                commonUtil.showMessage({message:result.message,type: result.msgLevel})
            }
        }
        else{
            commonUtil.showMessage({message:commonUtil.getMessageFromList("error.system",null),type: commonConstants.messageType.error})
        }
      })
      .catch(error => {
          //错误处理
        if (error && error.response)
        {
            switch (error.response.status) {
                case 400:commonUtil.showMessage({message:commonConstants.errorCodeMsg[400],type: commonConstants.messageType.error});break;
                case 401:commonUtil.showMessage({message:commonConstants.errorCodeMsg[401],type: commonConstants.messageType.error});break;
                case 403:commonUtil.showMessage({message:commonConstants.errorCodeMsg[403],type: commonConstants.messageType.error});break;
                case 404:commonUtil.showMessage({message:commonConstants.errorCodeMsg[404],type: commonConstants.messageType.error});break;
                case 408:commonUtil.showMessage({message:commonConstants.errorCodeMsg[408],type: commonConstants.messageType.error});break;
                case 500:commonUtil.showMessage({message:commonConstants.errorCodeMsg[500],type: commonConstants.messageType.error});break;
                case 501:commonUtil.showMessage({message:commonConstants.errorCodeMsg[501],type: commonConstants.messageType.error});break;
                case 502:commonUtil.showMessage({message:commonConstants.errorCodeMsg[502],type: commonConstants.messageType.error});break;
                case 503:commonUtil.showMessage({message:commonConstants.errorCodeMsg[503],type: commonConstants.messageType.error});break;
                case 504:commonUtil.showMessage({message:commonConstants.errorCodeMsg[504],type: commonConstants.messageType.error});break;
                default:commonUtil.showMessage({message:commonUtil.getMessageFromList("error.system",null),type: commonConstants.messageType.error});
            }
        }
      })
      .finally(() => {
        // this.loading = false;
      });
}

//发送异步post请求
//url:请求后台的url
//params:参数 map格式
//callback:回调函数
//removeEmpty:空字符串是否作为参数传递到后台 false:空字符串将传递到后台 true:空字符串将不会传递到后台，后台对应的值为null
commonUtil.doAjaxPost = function(obj)
{
    if(obj.removeEmpty)
    {
        commonUtil.removeEmpty(obj.params);
    }
    
    Axios.post(obj.url,obj.params,{headers: {'Content-Type': 'application/json',
    'Authorization':sessionStorage.getItem("token")}})
    .then(response => {
        console.log("成功报文:", response);
        if(response.status == 200)
        {
              //请求成功
            var result = response.data;
            if (result.statusCode == "200")
            {
                if(result.message)
                {
                commonUtil.showMessage({message:result.message,type: result.msgLevel})
                }
            }else{
                if(result.statusCode == "50004")
                {
                    sessionStorage.removeItem('token');
                    router.replace('/login');
                    commonUtil.showMessage({message:result.message,type: result.msgLevel})
                }else{
                    if(result.message)
                    {
                    commonUtil.showMessage({message:result.message,type: result.msgLevel})
                    }
                }
                
            }
            if(obj.callback)
            {
                if(obj.that)
                {
                    result.that = obj.that;
                }
                if(obj.init)
                {
                    result.init = obj.init;
                }
                obj.callback(result)
            }
        }else{
            commonUtil.showMessage({message:commonUtil.getMessageFromList("error.system",null),type: commonConstants.messageType.error})
        }
      })
      .catch(error => {
        //错误处理
        if (error && error.response)
        {
            switch (error.response.status) {
                case 400:commonUtil.showMessage({message:commonConstants.errorCodeMsg[400],type: commonConstants.messageType.error});break;
                case 401:commonUtil.showMessage({message:commonConstants.errorCodeMsg[401],type: commonConstants.messageType.error});break;
                case 403:commonUtil.showMessage({message:commonConstants.errorCodeMsg[403],type: commonConstants.messageType.error});break;
                case 404:commonUtil.showMessage({message:commonConstants.errorCodeMsg[404],type: commonConstants.messageType.error});break;
                case 408:commonUtil.showMessage({message:commonConstants.errorCodeMsg[408],type: commonConstants.messageType.error});break;
                case 500:commonUtil.showMessage({message:commonConstants.errorCodeMsg[500],type: commonConstants.messageType.error});break;
                case 501:commonUtil.showMessage({message:commonConstants.errorCodeMsg[501],type: commonConstants.messageType.error});break;
                case 502:commonUtil.showMessage({message:commonConstants.errorCodeMsg[502],type: commonConstants.messageType.error});break;
                case 503:commonUtil.showMessage({message:commonConstants.errorCodeMsg[503],type: commonConstants.messageType.error});break;
                case 504:commonUtil.showMessage({message:commonConstants.errorCodeMsg[504],type: commonConstants.messageType.error});break;
                default:commonUtil.showMessage({message:commonUtil.getMessageFromList("error.system",null),type: commonConstants.messageType.error});
            }
        }
      })
}

//发送异步get请求
//url:请求后台的url
//params:参数 map格式
//callback:回调函数
//removeEmpty:空字符串是否作为参数传递到后台 false:空字符串将传递到后台 true:空字符串将不会传递到后台，后台对应的值为null
commonUtil.doAjaxGet = function(obj)
{
    if(obj.removeEmpty)
    {
        commonUtil.removeEmpty(obj.params);
    }
    Axios.get(obj.url,{
        params:obj.params,
        headers: {'Authorization':sessionStorage.getItem("token")}
    })
      .then((response) => {
        if(response.status == 200)
        {
              //请求成功
            var result = response.data;
            if (result.statusCode == "200")
            {
                if(result.message)
                {
                    commonUtil.showMessage({message:result.message,type: result.msgLevel})
                }
            }else{
                if(result.statusCode == "50004")
                {
                    sessionStorage.removeItem('token');
                    router.replace('/login');
                    commonUtil.showMessage({message:result.message,type: result.msgLevel})
                }else{
                    if(result.message)
                    {
                    commonUtil.showMessage({message:result.message,type: result.msgLevel})
                    }
                }
                
            }
            if(obj.callback)
            {
                if(obj.that)
                {
                    result.that = obj.that;
                }
                if(obj.init)
                {
                    result.init = obj.init;
                }
                obj.callback(result)
            }
        }else{
            commonUtil.showMessage({message:commonUtil.getMessageFromList("error.system",null),type: commonConstants.messageType.error})
        }
      })
      .catch((error) => {
        //错误处理
        if (error && error.response)
        {
            switch (error.response.status) {
                case 400:commonUtil.showMessage({message:commonConstants.errorCodeMsg[400],type: commonConstants.messageType.error});break;
                case 401:commonUtil.showMessage({message:commonConstants.errorCodeMsg[401],type: commonConstants.messageType.error});break;
                case 403:commonUtil.showMessage({message:commonConstants.errorCodeMsg[403],type: commonConstants.messageType.error});break;
                case 404:commonUtil.showMessage({message:commonConstants.errorCodeMsg[404],type: commonConstants.messageType.error});break;
                case 408:commonUtil.showMessage({message:commonConstants.errorCodeMsg[408],type: commonConstants.messageType.error});break;
                case 500:commonUtil.showMessage({message:commonConstants.errorCodeMsg[500],type: commonConstants.messageType.error});break;
                case 501:commonUtil.showMessage({message:commonConstants.errorCodeMsg[501],type: commonConstants.messageType.error});break;
                case 502:commonUtil.showMessage({message:commonConstants.errorCodeMsg[502],type: commonConstants.messageType.error});break;
                case 503:commonUtil.showMessage({message:commonConstants.errorCodeMsg[503],type: commonConstants.messageType.error});break;
                case 504:commonUtil.showMessage({message:commonConstants.errorCodeMsg[504],type: commonConstants.messageType.error});break;
                default:commonUtil.showMessage({message:commonUtil.getMessageFromList("error.system",null),type: commonConstants.messageType.error});
            }
        }
      });
}
commonUtil.download = function(url,params,callback)
{
    Axios.post(url,params,{   
        responseType: 'blob',            
        },            
    ).then((response=>{                
            const url = window.URL.createObjectURL(new Blob([response.data]));                
            const link = document.createElement('a');                
            link.href = url;   
            if(response.headers.filename)
            {
                link.setAttribute('download', response.headers.filename);         
            }else{
                link.setAttribute('download', commonUtil.getUuid());
            }            
            document.body.appendChild(link);                
            link.click();   
            if(callback)
            {
                callback(response)
            }         
        })
    ).catch((error) => {
        //错误处理
        if (error && error.response)
        {
            const errormsg = error.response.headers.errormsg;
            if(errormsg)
            {
                commonUtil.showMessage({message:decodeURI(errormsg),type: commonConstants.messageType.error});
            }else{
                commonUtil.showMessage({message:commonUtil.getMessageFromList("error.system",null),type: commonConstants.messageType.error});
            }
            
        }
      });
}
//确认消息
//confirmButtonText:确认按钮显示内容 cancelButtonText：取消按钮显示内容，
//api：请求的url requestParam：请求参数 callback：请求成功后的回调函数 type:请求方式，get 或者post，默认为post
commonUtil.showConfirm = function(params)
{
    MessageBox.confirm(params.messageContent,
    {
        confirmButtonText:params.confirmButtonText?params.confirmButtonText:"确认",
        cancelButtonText: params.cancelButtonText?params.cancelButtonText:'取消',
        type:commonConstants.messageType.warning,
    }).then(() => {
    if(params.type && params.type.toLowerCase() == "get")
    {
        var object = {
            url:params.url,
            params:params.requestParam,
            removeEmpty:false,
            callback:params.callback,
            that:params.that
        }
        commonUtil.doAjaxGet(object);
    }else{
        var object = {
            url:params.url,
            params:params.requestParam,
            removeEmpty:false,
            callback:params.callback,
            that:params.that
        }
        commonUtil.doAjaxPost(object);
    }
        
    }).catch(() => {
        // commonUtil.showMessage({message:commonUtil.getMessageFromList("error.delete",null),type: commonConstants.messageType.error})        
    });
}
//清空map对象
commonUtil.clearObj = function(obj)
{
    for(var i in obj) {
       if(obj[i]  instanceof Array)
       {
        obj[i] = [];
       }else{
        obj[i] = "";
       }

    }
}

commonUtil.removeEmpty = function (obj)
{
    for(var i in obj)
    {
        if(obj[i] == "")
        {
            delete obj[i]
        }
    }
}

//数据数据字典值对应的name
commonUtil.getDictionaryValueName = function(type,value)
{
    if(commonConstants.dictionary[type])
    {
        return commonConstants.dictionary[type][value];
    }else{
        return "-";
    }
    
    // console.log(commonConstants.dictionary[type][value])
}
//获取uuid
commonUtil.getUuid = function()
{
    var len = 32;//32长度
    var radix = 16;//16进制
    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
    var uuid = [], i;
    radix = radix || chars.length;
    if(len) {
      for(i = 0; i < len; i++)uuid[i] = chars[0 | Math.random() * radix];
    } else {
      var r;
      uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
      uuid[14] = '4';
      for(i = 0; i < 36; i++) {
        if(!uuid[i]) {
          r = 0 | Math.random() * 16;
          uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
        }
      }
    }
    return uuid.join('');
}
//关闭页面
commonUtil.closeTag=function(name,that){
    var tag={
        name:name,
      }
      that.GLOBAL.tagsList.splice(that.GLOBAL.tagsList.indexOf(tag), 1);
      that.$router.push("/index");
}
//对象赋值
commonUtil.coperyProperties = function(target,source)
{
    if(source.id)
    {
        target.id = source.id;
    }
    for(var i in target) {
        target[i] = source[i];
    }
}
export default commonUtil;
