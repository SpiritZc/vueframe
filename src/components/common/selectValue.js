//下拉选项用共通js
const selectUtil = {
 
}
//数据库类型下拉选项
selectUtil.databaseTypeList=[
    {value:"1",label:"mysql"},
    {value:"2",label:"oracle"}
]

selectUtil.yesNo=[
    {value:"1",label:"是"},
    {value:"2",label:"否"},
]

selectUtil.checkRule=[
    {value:"1",label:"公开访问"},
    {value:"2",label:"登陆后访问"},
    {value:"3",label:"登陆后并授权访问"},
]

selectUtil.accesskRule=[
    {value:"1",label:"登陆后访问"},
    {value:"2",label:"登陆后并授权访问"},
]

selectUtil.weekday=[
    {value:"1",label:"星期一"},
    {value:"2",label:"星期二"},
    {value:"3",label:"星期三"},
    {value:"4",label:"星期四"},
    {value:"5",label:"星期五"},
    {value:"6",label:"星期六"},
    {value:"7",label:"星期日"},
]
selectUtil.holidayType=[
    {value:"1",label:"法定假期"},
    {value:"2",label:"周末"},
]

selectUtil.driverClass=[
    {value:"com.mysql.jdbc.Driver",label:"com.mysql.jdbc.Driver"},
    {value:"com.mysql.cj.jdbc.Driver",label:"com.mysql.cj.jdbc.Driver"},
]

selectUtil.chartType=[
    {value:"01",label:"柱状图"},
    {value:"02",label:"饼状图"},
    {value:"03",label:"折线图"},
]

export default selectUtil;