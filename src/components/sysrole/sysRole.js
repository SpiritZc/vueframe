import { mapState, mapGetters, mapActions, mapMutations } from 'vuex'
export default {
  name:'sysRole',
  data() {
    return{
      pageData:{
        that:this,
        //请求的url start
        requestUrl:{//请求的url
          listApi:"/api/sysRole/getTableList",//获取表格数据api
          insertApi:"/api/sysRole/insert",//新增用api
          updateApi:"/api/sysRole/update",//更新用api
          getDetailApi:"/api/sysRole//getDetail",//获取详情用api
          deleteOneApi:"/api/sysRole/delete",//单条删除api
          deleteBatchApi:"/api/sysRole/deletebatch",//批量删除api
          getAuthTreeApi:"/api/sysMenu/getAuthTree",//获取权限树api
          authApi:"/api/sysRole/authed",//授权
        },
        //请求的url end
        //查询表单内容 start
        searchForm:[
          {type:'Input',label:'角色代码',prop:'roleCode'},
          {type:'Input',label:'角色名称',prop:'roleName'},
        ],
        //查询表单内容 end
        //查询条件 start
        queryData:{
          roleCode:"",//角色代码
          roleName:"",//角色名称
        },
        //查询条件 end
        //查询表单按钮start
        searchHandle:[
          {label:'查询',type:'primary',handle:()=>this.searchtablelist(this.pageData),auth:'sysRole_search'},
          {label:'重置',type:'warning',handle:()=>this.reset(this.pageData),auth:'sysRole_search'}
        ],
        //查询表单按钮end
        //表格数据start
        tableData:[],
        //表格数据end
        //表格工具栏按钮 start
        tableHandles:[
          {label:'新增',type:'primary',handle:()=>this.showInsert({pageData:this.pageData,removeEmpty:false}),auth:'sysRole_insert'},
          {label:'批量删除',type:'danger',handle:()=>this.deleteBatch({pageData:this.pageData}),auth:'sysRole_batchdelete'}
        ],
        //表格工具栏按钮 end
        selectList:[],//表格选中的数据
        //表格分页信息start
        tablePage:{
          currentPage: 1,
          pageSize:10,
          pageTotal: 0,
          pageSizeRange:[5, 10, 20, 50]
        },
        //表格分页信息end
        //表格列表头start
        tableCols:[
          {label:'角色代码',prop:'roleCode',align:'center'},
          {label:'角色名称',prop:'roleName',align:'center'},
          {label:'角色描述',prop:'roleDesc',align:'center'},
          {label:'操作',prop:'operation',align:'center',type:'button',btnList:[
            {label:'查看',type:'text',auth:'sysRole_getdetail',handle:(that,row)=>this.showDetail({pageData:this.pageData,param:{id:row.id},callback:this.getDetailBack})},
            {label:'编辑',type:'text',auth:'sysRole_update',handle:(that,row)=>this.showEdit({pageData:this.pageData,param:{id:row.id},removeEmpty:false,callback:this.getDetailBack})},
            {label:'权限配置',type:'text',auth:'sysRole_showauth',handle:(that,row)=>this.showAuthModal(row.id)},
            {label:'删除',type:'text',auth:'sysRole_delete',handle:(that,row)=>this.delete({pageData:this.pageData,param:{id:row.id}})},
          ]}
        ],
        //表格列表头end
        //modal配置 start
        modalConfig:{ 
          title: "新增", //弹窗标题,值为:新增，查看，编辑
          show: false, //弹框显示
          formEditDisabled:false,//编辑弹窗是否可编辑
          width:'700px',//弹出框宽度
          type:'1',//类型 1新增 2编辑 3详情
          removeEmpty:false,//空字符串是否作为参数传递到后台 false:空字符串将传递到后台 true:空字符串将不会传递到后台，后台对应的值为null
          submitUrl:'',//提交按钮对应的url
          disabled:false,//按钮是否可用
          modalRef:"modalRef",//modal标识
        },
        //modal配置 end
        //modal表单 start
        modalForm:[
          {type:'Input',label:'角色代码',prop:'roleCode',rules:{required:true,maxLength:20}},
          {type:'Input',label:'角色名称',prop:'roleName',rules:{required:true,maxLength:40}},
          {type:'Input',label:'角色描述',prop:'roleDesc',rules:{required:true,maxLength:100}},
        ],
        //modal表单 end
        //modal 数据 start
        modalData : {//modal页面数据
          roleCode:"",//角色代码 
          roleName:"",//角色名称 
          roleDesc:"",//角色描述 
          authed:[],//已授权的数据
        },
        //modal 数据 end
        //modal 按钮 start
        modalHandles:[
          {label:'取消',type:'default',handle:()=>this.closeModal(this.pageData)},
          {label:'提交',type:'primary',handle:()=>this.save({pageData:this.pageData})}
        ],
        //modal 按钮 end
        //授权页面配置 start
        authDialogParam:{//modal页面标题、是否显示等参数
          title: "权限配置", //弹窗标题,值为:新增，查看，编辑
          show: false, //弹框显示,
          type:"1"
        },
        authModalForm:[
          {type:'Tree',data:[],checked:[],props:{children: 'children',label: 'menuName'},ref:"tree",click:this.check},
        ],
        authModalHandles:[
          {label:'取消',type:'default',handle:()=>this.closeAuthModal()},
          {label:'提交',type:'primary',handle:()=>this.auth()}
        ],
        authModalData:{
          id:"",//角色id
          authed:[],//已授权的数据
        },
      }
    }
  },
  mounted(){
    this.searchtablelist(this.pageData);
  },
  methods:{
    ...mapActions([
      'searchtablelist','showModal','save','closeModal','showDetail','delete','saveCallback','showDetailCallback',
      'showEdit','showEditCallback','showInsert','deleteCallback','selectChange','deleteBatch','reset'
    ]),
    showAuthModal(id){//显示权限配置页面
      this.pageData.authDialogParam.show = true;//显示弹框
      this.pageData.authModalData.id = id;
      this.getAuthTree(id);//获取权限树
    },
    getAuthTree(id){//获取菜单树
      let param = {
        url:this.pageData.requestUrl.getAuthTreeApi,
        params:{id:id},
        removeEmpty:false,
        callback:this.getAuthTreeCallback
      }
      this.commonUtil.doAjaxPost(param)
    },
    getAuthTreeCallback(data)
    {
      this.pageData.authModalForm[0].data = data.responseData.treeData;
      this.pageData.authModalForm[0].checked = data.responseData.authed;
    },
    closeAuthModal(){//关闭权限配置页面
      this.pageData.authDialogParam.show = false;//关闭弹框
      this.pageData.authModalForm[0].data = [];
      this.pageData.authModalForm[0].checked = [];
    },
    check(data,keys){
      this.pageData.authModalData.authed = keys.checkedKeys.concat(keys.halfCheckedKeys);
    },
    auth(){//确定授权
      if(this.pageData.authModalData.authed.length == 0)
      {
        this.commonUtil.showMessage({message:this.commonUtil.getMessageFromList("error.auth.select",null),type: this.commonConstants.messageType.error});
      }else{
        var obj = {
          url:this.pageData.requestUrl.authApi,
          params:this.pageData.authModalData,
          removeEmpty:false,
          callback:this.closeAuthModal,
        }
        this.commonUtil.doAjaxPost(obj)
      }
    }
  }
};