import { mapState, mapGetters, mapActions, mapMutations } from 'vuex'
export default {
  name:'sysUser',
  data() {
    return{
      pageData:{
        that:this,
        //请求的url start
        requestUrl:{
          listApi:"/api/sysMenu/getTableList",//获取表格数据api
          insertApi:"/api/sysMenu/insert",//新增用api
          updateApi:"/api/sysMenu/update",//更新用api
          getDetailApi:"/api/sysMenu//getDetail",//获取详情用api
          deleteOneApi:"/api/sysMenu/delete",//单条删除api
          deleteBatchApi:"/api/sysMenu/deletebatch",//批量删除api
          getMenuTreeApi:"/api/sysMenu/getMenuTree",//获取菜单树
        },
        //请求的url end
        //查询表单内容 start
        searchForm:[
          {type:'Input',label:'菜单名称',prop:'menuName'},
          {type:'Input',label:'上级菜单',prop:'parentMenuName'},
        ],
        //查询表单内容 end
        //查询条件 start
        queryData:{
          menuName:"",//菜单名称
          menuUrl:"",//菜单url
          menuIcon:"",//菜单图标
          parentMenuName:"",//上级菜单
        },
        //查询条件 end
        //查询表单按钮start
        searchHandle:[
          {label:'查询',type:'primary',handle:()=>this.searchtablelist(this.pageData),auth:'sysMenu_search'},
          {label:'重置',type:'warning',handle:()=>this.reset(this.pageData),auth:'sysMenu_search'}
        ],
        //查询表单按钮end
        //表格数据start
        tableData:[],
        //表格数据end
        //表格工具栏按钮 start
        tableHandles:[
          {label:'新增',type:'primary',handle:()=>this.showInsert({pageData:this.pageData,removeEmpty:false,init:this.getMenuTree}),auth:'sysMenu_insert'},
          {label:'批量删除',type:'danger',handle:()=>this.deleteBatch({pageData:this.pageData}),auth:'sysMenu_batchdelete'}
        ],
        //表格工具栏按钮 end
        selectList:[],//表格选中的数据
        //表格分页信息start
        tablePage:{
          currentPage: 1,
          pageSize:10,
          pageTotal: 0,
          pageSizeRange:[5, 10, 20, 50]
        },
        //表格分页信息end
        //表格列表头start
        tableCols:[
          {label:'菜单名称',prop:'menuName',align:'center'},
          {label:'菜单地址',prop:'menuUrl',align:'center'},
          {label:'菜单图标',prop:'menuIcon',align:'center'},
          {label:'上级菜单',prop:'parentMenuName',align:'center'},
          {label:'访问规则',prop:'accessRule',align:'center',formatter:this.commonUtil.getCodeName},
          {label:'操作',prop:'operation',align:'center',type:'button',btnList:[
            {label:'查看',type:'text',auth:'sysMenu_getdetail',handle:(that,row)=>this.showDetail({pageData:this.pageData,param:{id:row.id},callback:this.getDetailBack,init:this.getMenuTree})},
            {label:'编辑',type:'text',auth:'sysMenu_update',handle:(that,row)=>this.showEdit({pageData:this.pageData,param:{id:row.id},removeEmpty:false,callback:this.getDetailBack,init:this.getMenuTree})},
            {label:'菜单接口',type:'text',auth:'sysMenu_update',handle:(that,row)=>this.routerTo(row)},
            {label:'删除',type:'text',auth:'sysMenu_delete',handle:(that,row)=>this.delete({pageData:this.pageData,param:{id:row.id}})},
          ]}
        ],
        //表格列表头end
        //modal配置 start
        modalConfig:{ 
          title: "新增", //弹窗标题,值为:新增，查看，编辑
          show: false, //弹框显示
          formEditDisabled:false,//编辑弹窗是否可编辑
          width:'700px',//弹出框宽度
          type:'1',//类型 1新增 2编辑 3详情
          removeEmpty:false,//空字符串是否作为参数传递到后台 false:空字符串将传递到后台 true:空字符串将不会传递到后台，后台对应的值为null
          submitUrl:'',//提交按钮对应的url
          disabled:false,//按钮是否可用
          modalRef:"modalRef",//modal标识
        },
        //modal配置 end
        //modal表单 start
        modalForm:[
          {type:'Input',label:'菜单名称',prop:'menuName',rules:{required:true,maxLength:20}},
          {type:'Input',label:'菜单地址',prop:'menuUrl',rules:{maxLength:200}},
          {type:'Input',label:'菜单图标',prop:'menuIcon',rules:{required:true,maxLength:50}},
          {type:'TreeSelect',label:'上级菜单',prop:'parentMenuId',rules:{required:false},props:{parent: 'parentMenuId',value: 'id',label: 'menuName',children: 'children'},data:[],ref:'select'},
          {type:'Select',label:'是否隐藏',prop:'isHidden',rules:{required:true},options:this.selectUtil.yesNo},
          {type:'Select',label:'访问规则',prop:'accessRule',rules:{required:true},options:this.selectUtil.accesskRule},
        ],
        //modal表单 end
        //modal 数据 start
        modalData : {//modal页面数据
          menuName:"",//菜单名称 
          menuUrl:"",//菜单url 
          parentMenuId:"",//上级菜单id，一级菜单用0表示 
          menuIcon:"",//菜单图标 
          isHidden:"",//是否隐藏菜单
          accessRule:"",//访问规则
        },
        //modal 数据 end
        //modal 按钮 start
        modalHandles:[
          {label:'取消',type:'default',handle:()=>this.closeModal(this.pageData)},
          {label:'提交',type:'primary',handle:()=>this.save({pageData:this.pageData})}
        ],
        //modal 按钮 end
        //其他参数 start
        roles:[],//角色
        //其他参数 end
      }
    }
  },
  mounted(){
    this.searchtablelist(this.pageData);
  },
  methods:{
    ...mapActions([
      'searchtablelist','showModal','save','closeModal','showDetail','delete','saveCallback','showDetailCallback',
      'showEdit','showEditCallback','showInsert','deleteCallback','selectChange','deleteBatch','reset'
    ]),
    getMenuTree(){//获取菜单树
      let param = {
        url:this.pageData.requestUrl.getMenuTreeApi,
        params:{},
        removeEmpty:false,
        callback:this.getMenuTreeCallback
      }
      this.commonUtil.doAjaxPost(param);
    },
    getMenuTreeCallback(data){
      this.$refs.modalRef.$refs.select[0].labelModel = "";
      this.pageData.modalForm[3].data = data.responseData
    },
    routerTo(row){
      this.$router.push({ name: 'sysApi',query:{menuId:row.id}})
    },
  }
};