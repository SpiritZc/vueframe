import { mapState, mapGetters, mapActions, mapMutations } from 'vuex'
export default {
  name:'sysRole',
  data() {
    return{
      pageData:{
        that:this,
        //请求的url start
        requestUrl:{//请求的url
          listApi:"/api/basicTemplateVariable/getTableList",//获取表格数据api
          insertApi:"/api/basicTemplateVariable/insert",//新增用api
          updateApi:"/api/basicTemplateVariable/update",//更新用api
          getDetailApi:"/api/basicTemplateVariable//getDetail",//获取详情用api
          deleteOneApi:"/api/basicTemplateVariable/delete",//单条删除api
          deleteBatchApi:"/api/basicTemplateVariable/deletebatch",//批量删除api
          getProjectList:"/api/basicProject/getAllProjects",//获取所有的项目信息
          getProjectTemplate:"/api/basicTemplate/getProjectTemplate",//获取所有的模板信息
        },
        //请求的url end
        //查询表单内容 start
        searchForm:[
          {type:'Select',label:'所属项目',prop:'projectId',props:{label:'projectName',value:'id'},change:this.changeProject,params:'1'},
          {type:'Select',label:'所属模板',prop:'templateId',props:{label:'templateName',value:'id'}},
          {type:'Input',label:'变量代码',prop:'variableCode'},
          {type:'Input',label:'变量名称',prop:'variableName'},
        ],
        //查询表单内容 end
        //查询条件 start
        queryData:{
          variableCode:"",    
          variableName:"",
          projectId:"",
          templateId:"",
        },
        //查询条件 end
        //查询表单按钮start
        searchHandle:[
          {label:'查询',type:'primary',handle:()=>this.searchtablelist(this.pageData),auth:'basicTemplateType_search'},
          {label:'重置',type:'warning',handle:()=>this.reset(this.pageData),auth:'basicTemplateType_search'}
        ],
        //查询表单按钮end
        //表格数据start
        tableData:[],
        //表格数据end
        //表格工具栏按钮 start
        tableHandles:[
          {label:'新增',type:'primary',handle:()=>this.showInsert({pageData:this.pageData,removeEmpty:false}),auth:'basicTemplateType_insert'},
          {label:'批量删除',type:'danger',handle:()=>this.deleteBatch({pageData:this.pageData}),auth:'basicTemplateType_batchdelete'}
        ],
        //表格工具栏按钮 end
        selectList:[],//表格选中的数据
        //表格分页信息start
        tablePage:{
          currentPage: 1,
          pageSize:10,
          pageTotal: 0,
          pageSizeRange:[5, 10, 20, 50]
        },
        //表格分页信息end
        //表格列表头start
        tableCols:[
          {label:'所属项目',prop:'projectName',align:'center'},
          {label:'所属模板',prop:'templateName',align:'center'},
          {label:'变量代码',prop:'variableCode',align:'center'},
          {label:'变量名称',prop:'variableName',align:'center'},
          {label:'默认值',prop:'variableValue',align:'center'},
          {label:'是否通用',prop:'isCommon',align:'center',formatter:this.commonUtil.getCodeName},
          {label:'是否静态',prop:'isStatic',align:'center',formatter:this.commonUtil.getCodeName},
          {label:'操作',prop:'operation',align:'center',type:'button',btnList:[
            {label:'查看',type:'text',auth:'basicTemplateType_getdetail',handle:(that,row)=>this.showDetail({pageData:this.pageData,param:{id:row.id},callback:this.getDetailBack})},
            {label:'编辑',type:'text',auth:'basicTemplateType_update',handle:(that,row)=>this.showEdit({pageData:this.pageData,param:{id:row.id},removeEmpty:false,callback:this.getDetailBack})},
            {label:'删除',type:'text',auth:'basicTemplateType_delete',handle:(that,row)=>this.delete({pageData:this.pageData,param:{id:row.id}})},
          ]}
        ],
        //表格列表头end
        //modal配置 start
        modalConfig:{ 
          title: "新增", //弹窗标题,值为:新增，查看，编辑
          show: false, //弹框显示
          formEditDisabled:false,//编辑弹窗是否可编辑
          width:'700px',//弹出框宽度
          type:'1',//类型 1新增 2编辑 3详情
          removeEmpty:false,//空字符串是否作为参数传递到后台 false:空字符串将传递到后台 true:空字符串将不会传递到后台，后台对应的值为null
          submitUrl:'',//提交按钮对应的url
          disabled:false,//按钮是否可用
          modalRef:"modalRef",//modal标识
        },
        //modal配置 end
        //modal表单 start
        modalForm:[
          {type:'Select',label:'是否通用',prop:'isCommon',rules:{required:true},options:this.selectUtil.yesNo,change:this.changeIsCommon},
          {type:'Select',label:'所属项目',prop:'projectId',rules:{required:true},props:{label:'projectName',value:'id'},change:this.changeProject,params:'2'},
          {type:'Select',label:'所属模板',prop:'templateId',rules:{required:true},props:{label:'templateName',value:'id'}},
          {type:'Input',label:'变量代码',prop:'variableCode',rules:{required:true,maxLength:30}},
          {type:'Input',label:'变量名称',prop:'variableName',rules:{required:true,maxLength:30}},
          {type:'Input',label:'默认值',prop:'variableValue',rules:{required:true,maxLength:50}},
          {type:'Select',label:'是否静态',prop:'isStatic',rules:{required:true},options:this.selectUtil.yesNo},
        ],
        //modal表单 end
        //modal 数据 start
        modalData : {//modal页面数据
          variableCode:"",    
          variableName:"",    
          variableValue:"",    
          isCommon:"", 
          projectId:"", 
          templateId:"", 
          isStatic:"", 
        },
        //modal 数据 end
        //modal 按钮 start
        modalHandles:[
          {label:'取消',type:'default',handle:()=>this.closeModal(this.pageData)},
          {label:'提交',type:'primary',handle:()=>this.save({pageData:this.pageData})}
        ],
        //modal 按钮 end
      }
    }
  },
  mounted(){
    this.searchtablelist(this.pageData);
    this.getProject();
  },
  methods:{
    ...mapActions([
      'searchtablelist','showModal','save','closeModal','showDetail','delete','saveCallback','showDetailCallback',
      'showEdit','showEditCallback','showInsert','deleteCallback','selectChange','deleteBatch','reset'
    ]),
    getProject(){//获取项目下拉框信息
      let param = {
            url:this.pageData.requestUrl.getProjectList,
            params:{},
            removeEmpty:false,
            callback:this.getProjectCallback
          }
          this.commonUtil.doAjaxPost(param)
    },
    getProjectCallback(data){
      this.pageData.modalForm[1].options = data.responseData;
      this.pageData.searchForm[0].options = data.responseData;
      this.$refs['searchForm'].$forceUpdate();//在methods中需强制更新，mounted中不需要
    },
    changeProject(projectId,type){//项目下拉框改变事件,获取项目下所有的模板信息
      if(projectId == null || projectId == "")
      {
        if(type == "1")
        {
          this.pageData.searchForm[1].options = null;
          this.pageData.queryData.templateId = "";
          this.$refs['searchForm'].$forceUpdate();//在methods中需强制更新，mounted中不需要
        }else{
          this.pageData.modalForm[2].options = [];
        }
      }else{
        var _self = this;
        let param = {
          url:this.pageData.requestUrl.getProjectTemplate,
          params:{projectId:projectId},
          removeEmpty:false,
          callback:function(data){
            if(type == "1")
            {
              _self.pageData.searchForm[1].options = data.responseData;
              _self.$refs['searchForm'].$forceUpdate();//在methods中需强制更新，mounted中不需要
              _self.pageData.queryData.templateId = "";
            }else{
              _self.pageData.modalForm[2].options = data.responseData;
              _self.pageData.modalData.templateId = "";
              _self.$refs['modalRef'].$forceUpdate();//在methods中需强制更新，mounted中不需要
            }
          }
        }
        this.commonUtil.doAjaxPost(param)
      }
      
    },
    changeIsCommon(){//是否共通修改
      if(this.pageData.modalData.isCommon == "1")
      {
        if(this.pageData.modalData.projectId)
        {
          this.changeProject(this.pageData.modalData.projectId,"2");
        }
        this.pageData.modalForm[2].rules.required = false;
        this.pageData.modalForm[2].show=false;
      }else{
        this.pageData.modalData.templateId = "";
        this.pageData.modalForm[2].rules.required = true;
        this.pageData.modalForm[2].show=true;
      }
    }
  }
};