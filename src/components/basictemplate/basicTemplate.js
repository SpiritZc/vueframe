import { mapState, mapGetters, mapActions, mapMutations } from 'vuex'
export default {
  name:'sysUser',
  data() {
    return{
      pageData:{
        that:this,
        //请求的url start
        requestUrl:{
          listApi:"/api/basicTemplate/getTableList",//获取表格数据api
          insertApi:"/api/basicTemplate/insert",//新增用api
          updateApi:"/api/basicTemplate/update",//更新用api
          getDetailApi:"/api/basicTemplate//getDetail",//获取详情用api
          deleteOneApi:"/api/basicTemplate/delete",//单条删除api
          deleteBatchApi:"/api/basicTemplate/deletebatch",//批量删除api
          getProjectList:"/api/basicProject/getAllProjects",//获取所有的项目信息
          getTemplateTypeApi:"/api/basicTemplateType/gettemplateTypeByProjectId",//获取项目模板类型
        },
        //请求的url end
        //查询表单内容 start
        searchForm:[
          {type:'Input',label:'模板名称',prop:'templateName'},
          {type:'Select',label:'所属项目',prop:'projectId',props:{label:'projectName',value:'id'}},
        ],
        //查询表单内容 end
        //查询条件 start
        queryData:{
          templateName:"", 
          projectId:"",
        },
        //查询条件 end
        //查询表单按钮start
        searchHandle:[
          {label:'查询',type:'primary',handle:()=>this.searchtablelist(this.pageData),auth:'basicTemplate_search'},
          {label:'重置',type:'warning',handle:()=>this.reset(this.pageData),auth:'basicTemplate_search'}
        ],
        //查询表单按钮end
        //表格数据start
        tableData:[],
        //表格数据end
        //表格工具栏按钮 start
        tableHandles:[
          {label:'新增',type:'primary',handle:()=>this.showInsert({pageData:this.pageData,removeEmpty:false}),auth:'basicTemplate_insert'},
          {label:'批量删除',type:'danger',handle:()=>this.deleteBatch({pageData:this.pageData}),auth:'basicTemplate_batchdelete'}
        ],
        //表格工具栏按钮 end
        selectList:[],//表格选中的数据
        //表格分页信息start
        tablePage:{
          currentPage: 1,
          pageSize:10,
          pageTotal: 0,
          pageSizeRange:[5, 10, 20, 50]
        },
        //表格分页信息end
        //表格列表头start
        tableCols:[
          {label:'模板地址',prop:'templateUrl',align:'center'},
          {label:'模板类型',prop:'typeName',align:'center'},
          {label:'模板名称',prop:'templateName',align:'center'},
          {label:'模板引擎',prop:'engineType',align:'center',formatter:this.commonUtil.getCodeName},
          {label:'所属项目',prop:'projectName',align:'center'},
          {label:'文件路径',prop:'templateFolderpath',align:'center'},
          {label:'操作',prop:'operation',align:'center',type:'button',btnList:[
            {label:'查看',type:'text',auth:'basicTemplate_getdetail',handle:(that,row)=>this.showDetail({pageData:this.pageData,param:{id:row.id},callback:this.getDetailBack})},
            {label:'编辑',type:'text',auth:'basicTemplate_update',handle:(that,row)=>this.showEdit({pageData:this.pageData,param:{id:row.id},removeEmpty:true,callback:this.getDetailBack})},
            {label:'删除',type:'text',auth:'basicTemplate_delete',handle:(that,row)=>this.delete({pageData:this.pageData,param:{id:row.id}})},
          ]}
        ],
        //表格列表头end
        //modal配置 start
        modalConfig:{ 
          title: "新增", //弹窗标题,值为:新增，查看，编辑
          show: false, //弹框显示
          formEditDisabled:false,//编辑弹窗是否可编辑
          width:'700px',//弹出框宽度
          type:'1',//类型 1新增 2编辑 3详情
          removeEmpty:false,//空字符串是否作为参数传递到后台 false:空字符串将传递到后台 true:空字符串将不会传递到后台，后台对应的值为null
          submitUrl:'',//提交按钮对应的url
          disabled:false,//按钮是否可用
          modalRef:"modalRef",//modal标识
        },
        //modal配置 end
        //modal表单 start
        modalForm:[
          {type:'Upload',label:'模板文件',tips:'上传模板文件',width:'500px',prop:'fileList',multiple:false,rules:{required:true},accept:"*.*"},
          {type:'Select',label:'所属项目',prop:'projectId',rules:{required:true},props:{label:'projectName',value:'id'},change:this.getTemplateType},
          {type:'Select',label:'模板类型',prop:'templateType',rules:{required:true},props:{label:'typeName',value:'id'},},
          {type:'Input',label:'文件路径',prop:'templateFolderpath',rules:{required:true,maxLength:200}},
        ],
        //modal表单 end
        //modal 数据 start
        modalData : {//modal页面数据
          templateUrl:"",    
          templateName:"",    
          projectId:"", 
          templateType:"",
          templateFolderpath:"",//文件路径
          fileList:[],//文件信息
        },
        //modal 数据 end
        //modal 按钮 start
        modalHandles:[
          {label:'取消',type:'default',handle:()=>this.closeModal(this.pageData)},
          {label:'提交',type:'primary',handle:()=>this.saveData({pageData:this.pageData})}
        ],
        //modal 按钮 end
        //其他参数 start
        //其他参数 end
      }
    }
  },
  mounted(){
    this.searchtablelist(this.pageData);
    this.getProject();
  },
  methods:{
    ...mapActions([
      'searchtablelist','showModal','save','closeModal','showDetail','delete','saveCallback','showDetailCallback',
      'showEdit','showEditCallback','showInsert','deleteCallback','selectChange','deleteBatch','reset'
    ]),
    getProject(){//获取项目下拉框信息
      let param = {
            url:this.pageData.requestUrl.getProjectList,
            params:{},
            removeEmpty:false,
            callback:this.getProjectCallback
          }
          this.commonUtil.doAjaxPost(param)
    },
    getProjectCallback(data){
      this.pageData.modalForm[1].options = data.responseData;
      this.pageData.searchForm[1].options = data.responseData;
      this.$refs['searchForm'].$forceUpdate();//在methods中需强制更新，mounted中不需要
    },
    getTemplateType(id){//获取模板类型
      if(id){
        let param = {
          url:this.pageData.requestUrl.getTemplateTypeApi,
          params:{projectId:id},
          removeEmpty:false,
          callback:this.getTemplateTypeCallback
        }
        this.commonUtil.doAjaxPost(param)
      }else{
        this.pageData.modalData.templateType = "";
        this.pageData.modalForm[2].options = [];
        this.$refs['modalRef'].$forceUpdate();//在methods中需强制更新，mounted中不需要
      }
      
    },
    getTemplateTypeCallback(data){
      this.pageData.modalForm[2].options = data.responseData;
      this.$refs['modalRef'].$forceUpdate();//在methods中需强制更新，mounted中不需要
    },
    saveData(){
      if(this.pageData.modalData.fileList.length>0)
      {
        this.pageData.modalData.templateName = this.pageData.modalData.fileList[0].name;
        this.pageData.modalData.templateUrl = this.pageData.modalData.fileList[0].url;
      }
      this.save({pageData:this.pageData})
    },
    getDetailBack(response)
    {
      if(response.statusCode == "200")
      {
        this.showModal({pageData:this.pageData,init:this.getProject})
        var responseData = response.responseData;
        this.commonUtil.coperyProperties(this.pageData.modalData,responseData);
        // this.pageData.modalData = responseData;
        let file = {
          name:responseData.templateName,
          url:responseData.templateUrl
        }
        this.pageData.modalData.fileList = new Array();
        this.pageData.modalData.fileList.push(file)
        if(this.pageData.modalData.projectId)
        {
          this.getTemplateType(this.pageData.modalData.projectId)
        }
      }
    }
  }
};