import { mapState, mapGetters, mapActions, mapMutations } from 'vuex'
export default {
  name:'sysRole',
  data() {
    return{
      pageData:{
        that:this,
        //请求的url start
        requestUrl:{//请求的url
          listApi:"/api/basicTemplateType/getTableList",//获取表格数据api
          insertApi:"/api/basicTemplateType/insert",//新增用api
          updateApi:"/api/basicTemplateType/update",//更新用api
          getDetailApi:"/api/basicTemplateType//getDetail",//获取详情用api
          deleteOneApi:"/api/basicTemplateType/delete",//单条删除api
          deleteBatchApi:"/api/basicTemplateType/deletebatch",//批量删除api
          getProjectList:"/api/basicProject/getAllProjects",//获取所有的项目信息
        },
        //请求的url end
        //查询表单内容 start
        searchForm:[
          {type:'Input',label:'类型代码',prop:'typeCode'},
          {type:'Input',label:'类型名称',prop:'typeName'},
          {type:'Input',label:'后缀名',prop:'typeSuffix'},
          {type:'Select',label:'所属项目',prop:'projectId',props:{label:'projectName',value:'id'}},
        ],
        //查询表单内容 end
        //查询条件 start
        queryData:{
          typeCode:"",//类型代码
          typeName:"",//类型名称
          typeSuffix:"",//后缀名
          projectId:"",//项目id
        },
        //查询条件 end
        //查询表单按钮start
        searchHandle:[
          {label:'查询',type:'primary',handle:()=>this.searchtablelist(this.pageData),auth:'basicTemplateType_search'},
          {label:'重置',type:'warning',handle:()=>this.reset(this.pageData),auth:'basicTemplateType_search'}
        ],
        //查询表单按钮end
        //表格数据start
        tableData:[],
        //表格数据end
        //表格工具栏按钮 start
        tableHandles:[
          {label:'新增',type:'primary',handle:()=>this.showInsert({pageData:this.pageData,removeEmpty:false}),auth:'basicTemplateType_insert'},
          {label:'批量删除',type:'danger',handle:()=>this.deleteBatch({pageData:this.pageData}),auth:'basicTemplateType_batchdelete'}
        ],
        //表格工具栏按钮 end
        selectList:[],//表格选中的数据
        //表格分页信息start
        tablePage:{
          currentPage: 1,
          pageSize:10,
          pageTotal: 0,
          pageSizeRange:[5, 10, 20, 50]
        },
        //表格分页信息end
        //表格列表头start
        tableCols:[
          {label:'所属项目',prop:'projectName',align:'center'},
          {label:'类型代码',prop:'typeCode',align:'center'},
          {label:'类型名称',prop:'typeName',align:'center'},
          {label:'文件名称',prop:'fileName',align:'center'},
          {label:'后缀名',prop:'typeSuffix',align:'center'},
          {label:'操作',prop:'operation',align:'center',type:'button',btnList:[
            {label:'查看',type:'text',auth:'basicTemplateType_getdetail',handle:(that,row)=>this.showDetail({pageData:this.pageData,param:{id:row.id},callback:this.getDetailBack})},
            {label:'编辑',type:'text',auth:'basicTemplateType_update',handle:(that,row)=>this.showEdit({pageData:this.pageData,param:{id:row.id},removeEmpty:false,callback:this.getDetailBack})},
            {label:'删除',type:'text',auth:'basicTemplateType_delete',handle:(that,row)=>this.delete({pageData:this.pageData,param:{id:row.id}})},
          ]}
        ],
        //表格列表头end
        //modal配置 start
        modalConfig:{ 
          title: "新增", //弹窗标题,值为:新增，查看，编辑
          show: false, //弹框显示
          formEditDisabled:false,//编辑弹窗是否可编辑
          width:'700px',//弹出框宽度
          type:'1',//类型 1新增 2编辑 3详情
          removeEmpty:false,//空字符串是否作为参数传递到后台 false:空字符串将传递到后台 true:空字符串将不会传递到后台，后台对应的值为null
          submitUrl:'',//提交按钮对应的url
          disabled:false,//按钮是否可用
          modalRef:"modalRef",//modal标识
        },
        //modal配置 end
        //modal表单 start
        modalForm:[
          {type:'Input',label:'类型代码',prop:'typeCode',rules:{required:true,maxLength:20}},
          {type:'Input',label:'类型名称',prop:'typeName',rules:{required:true,maxLength:40}},
          {type:'Input',label:'文件名称',prop:'fileName',rules:{required:true,maxLength:100}},
          {type:'Input',label:'后缀名',prop:'typeSuffix',rules:{required:true,maxLength:10}},
          {type:'Select',label:'所属项目',prop:'projectId',rules:{required:true},props:{label:'projectName',value:'id'}},
        ],
        //modal表单 end
        //modal 数据 start
        modalData : {//modal页面数据
          projectId:"",//项目id 
          typeCode:"",//类型代码 
          typeName:"",//类型名称 
          typeSuffix:"",//后缀名 
          fileName:"",//文件名称
        },
        //modal 数据 end
        //modal 按钮 start
        modalHandles:[
          {label:'取消',type:'default',handle:()=>this.closeModal(this.pageData)},
          {label:'提交',type:'primary',handle:()=>this.save({pageData:this.pageData})}
        ],
        //modal 按钮 end
      }
    }
  },
  mounted(){
    this.searchtablelist(this.pageData);
    this.getProject();
  },
  methods:{
    ...mapActions([
      'searchtablelist','showModal','save','closeModal','showDetail','delete','saveCallback','showDetailCallback',
      'showEdit','showEditCallback','showInsert','deleteCallback','selectChange','deleteBatch','reset'
    ]),
    getProject(){//获取项目下拉框信息
      let param = {
            url:this.pageData.requestUrl.getProjectList,
            params:{},
            removeEmpty:false,
            callback:this.getProjectCallback
          }
          this.commonUtil.doAjaxPost(param)
    },
    getProjectCallback(data){
      this.pageData.modalForm[4].options = data.responseData;
      this.pageData.searchForm[3].options = data.responseData;
      this.$refs['searchForm'].$forceUpdate();//在methods中需强制更新，mounted中不需要
    },
  }
};