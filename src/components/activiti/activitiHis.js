export default {
  name:'activitiHis',
  data() {
    return{
      pageData:{
        that:this,
        //请求的url start
        requestUrl:{
          getHisApi:"/api/activiti/getTaskHistory",//获取审批记录
          imageApi:"/api/activiti/taskImage",
          
        },
        datas:[],
      }
    }
  },
  mounted(){
    this.getTaskHis()
  },
  methods:{
    getTaskHis(){
        var businessKey = this.$route.query['businessKey'];
        let param = {
            url:this.pageData.requestUrl.getHisApi,
            params:{businessKey:businessKey},
            removeEmpty:false,
            callback:this.getTaskHisCallback
        }
        this.commonUtil.doAjaxPost(param);
    },
    getTaskHisCallback(response){
        var responseData = response.responseData;
        this.pageData.datas = responseData;
        var businessKey = this.$route.query['businessKey'];
        let image = document.getElementById('image');
        image.src = this.pageData.requestUrl.imageApi + "?businessKey="+businessKey;
        
    }
  },
};