import { mapState, mapGetters, mapActions, mapMutations } from 'vuex'
export default {
  name:'activiti',
  data() {
    return{
      pageData:{
        that:this,
        //请求的url start
        requestUrl:{
          listApi:"/api/activiti/getTableList",//获取表格数据api
          deployApi:"/api/activiti/deploy",//部署流程用api
          deleteOneApi:"/api/activiti/delete",//单条删除api
          deleteBatchApi:"/api/activiti/deletebatch",//批量删除api
          createApi:"/api/activiti/create",//新建模型api
          editApi:"/api/activiti/edit",//修改用api
          imageApi:"/api/activiti/image",//查看流程图用api
        },
        //请求的url end
        //查询表单内容 start
        searchForm:[
          {type:'Input',label:'流程名称',prop:'name'},
          {type:'Input',label:'流程标识',prop:'key'},
        ],
        //查询表单内容 end
        //查询条件 start
        queryData:{
            name:"",//流程名称
            key:"",//流程标识
        },
        //查询条件 end
        //查询表单按钮start
        searchHandle:[
          {label:'查询',type:'primary',handle:()=>this.searchtablelist(this.pageData),auth:'activiti_search'},
          {label:'重置',type:'warning',handle:()=>this.reset(this.pageData),auth:'activiti_search'}
        ],
        //查询表单按钮end
        //表格数据start
        tableData:[],
        //表格数据end
        //表格工具栏按钮 start
        tableHandles:[
          {label:'新增',type:'primary',handle:()=>this.showModal('1'),auth:'activiti_insert'},
          {label:'批量删除',type:'danger',handle:()=>this.deleteBatch({pageData:this.pageData}),auth:'activiti_batchdelete'}
        ],
        //表格工具栏按钮 end
        selectList:[],//表格选中的数据
        //表格分页信息start
        tablePage:{
          currentPage: 1,
          pageSize:10,
          pageTotal: 0,
          pageSizeRange:[5, 10, 20, 50]
        },
        //表格分页信息end
        //表格列表头start
        tableCols:[
          {label:'流程名称',prop:'name',align:'center'},
          {label:'流程标识',prop:'key',align:'center'},
          {label:'部署ID',prop:'deploymentId',align:'center'},
          {label:'版本',prop:'version',align:'center'},
          {label:'创建时间',prop:'createTime',align:'center'},
          {label:'最后更新时间',prop:'lastUpdateTime',align:'center'},
          {label:'操作',prop:'operation',align:'center',type:'button',btnList:[
            {label:'部署',type:'text',auth:'activiti_update',handle:(that,row)=>this.deploy(row.id)},
            {label:'修改',type:'text',auth:'activiti_update',handle:(that,row)=>this.showModal('2',row.id)},
            {label:'流程图',type:'text',auth:'activiti_update',handle:(that,row)=>this.showImage(row.id,row.deploymentId)},
            {label:'删除',type:'text',auth:'activiti_delete',handle:(that,row)=>this.delete({pageData:this.pageData,param:{id:row.id}})},
          ]}
        ],
        //表格列表头end
        //modal配置 start
        modalConfig:{ 
          title: "新增", //弹窗标题,值为:新增，查看，编辑
          show: false, //弹框显示
          formEditDisabled:false,//编辑弹窗是否可编辑
          width:'700px',//弹出框宽度
          type:'1',//类型 1新增 2编辑 3详情
          removeEmpty:false,//空字符串是否作为参数传递到后台 false:空字符串将传递到后台 true:空字符串将不会传递到后台，后台对应的值为null
          submitUrl:'',//提交按钮对应的url
          disabled:false,//按钮是否可用
          modalRef:"modalRef",//modal标识
        },
        imageModalConfig:{ 
          title: "新增", //弹窗标题,值为:新增，查看，编辑
          show: false, //弹框显示
          formEditDisabled:false,//编辑弹窗是否可编辑
          width:'700px',//弹出框宽度
          type:'1',//类型 1新增 2编辑 3详情
          removeEmpty:false,//空字符串是否作为参数传递到后台 false:空字符串将传递到后台 true:空字符串将不会传递到后台，后台对应的值为null
          submitUrl:'',//提交按钮对应的url
          disabled:false,//按钮是否可用
          modalRef:"modalRef",//modal标识
        },
        //modal配置 end
      }
    }
  },
  mounted(){
    this.searchtablelist(this.pageData);
  },
  methods:{
    ...mapActions([
      'searchtablelist','showModal','save','closeModal','showDetail','delete','saveCallback','showDetailCallback',
      'showEdit','showEditCallback','showInsert','deleteCallback','selectChange','deleteBatch','reset'
    ]),
    searchTableData(){
      this.searchtablelist(this.pageData)
    },
    //部署流程
    deploy(id){
        let param = {
            url:this.pageData.requestUrl.deployApi,
            params:{id:id},
            removeEmpty:false,
            callback:this.searchTableData
          }
          this.commonUtil.doAjaxGet(param)
    },
    showModal(type,id){
      if(type=="1")
      {
        this.pageData.modalConfig.title = "新增";
        this.pageData.modalConfig.show = true;
        let modalFrame = document.getElementById('modalFrame');
        modalFrame.src = this.pageData.requestUrl.createApi;
      }else{
        this.pageData.modalConfig.title = "修改";
        this.pageData.modalConfig.show = true;
        let modalFrame = document.getElementById('modalFrame');
        modalFrame.src = this.pageData.requestUrl.editApi + "?id="+id;
      }
    },
    closeDialog(){
      this.pageData.modalConfig.show = false;
      this.searchtablelist(this.pageData)
    },
    showImage(id,deploymentId){
      this.pageData.imageModalConfig.title = "流程图";
      this.pageData.imageModalConfig.show = true;
      let image = document.getElementById('image');
      image.src = this.pageData.requestUrl.imageApi + "?id="+id+"&deploymentId="+deploymentId;
    }
  },
};