import { mapState, mapGetters, mapActions, mapMutations } from 'vuex'
export default {
  name:'sysUser',
  data() {
    return{
      pageData:{
        that:this,
        //请求的url start
        requestUrl:{
          listApi:"/api/sysApi/getTableList",//获取表格数据api
          insertApi:"/api/sysApi/insert",//新增用api
          updateApi:"/api/sysApi/update",//更新用api
          getDetailApi:"/api/sysApi//getDetail",//获取详情用api
          deleteOneApi:"/api/sysApi/delete",//单条删除api
          deleteBatchApi:"/api/sysApi/deletebatch",//批量删除api
        },
        //请求的url end
        //查询表单内容 start
        searchForm:[
          {type:'Input',label:'接口标识',prop:'apiCode'},
          {type:'Input',label:'接口名称',prop:'apiName'},
          {type:'Input',label:'接口功能',prop:'apiFunction'},
        ],
        //查询表单内容 end
        //查询条件 start
        queryData:{
          menuId:"",
          apiCode:"",//接口唯一标识
          apiName:"",//接口名称
          apiFunction:"",//接口功能
          apiUrl:"",//接口连接
          isDefault:"",//是否默认 1是 2否
        },
        //查询条件 end
        //查询表单按钮start
        searchHandle:[
          {label:'查询',type:'primary',handle:()=>this.searchtablelist(this.pageData),auth:'sysApi_search'},
          {label:'重置',type:'warning',handle:()=>this.reset(this.pageData),auth:'sysApi_search'}
        ],
        //查询表单按钮end
        //表格数据start
        tableData:[],
        //表格数据end
        //表格工具栏按钮 start
        tableHandles:[
          {label:'新增',type:'primary',handle:()=>this.showInsert({pageData:this.pageData,removeEmpty:false,init:this.getRoles}),auth:'sysApi_insert'},
          {label:'批量删除',type:'danger',handle:()=>this.deleteBatch({pageData:this.pageData}),auth:'sysApi_batchdelete'}
        ],
        //表格工具栏按钮 end
        selectList:[],//表格选中的数据
        //表格分页信息start
        tablePage:{
          currentPage: 1,
          pageSize:10,
          pageTotal: 0,
          pageSizeRange:[5, 10, 20, 50]
        },
        //表格分页信息end
        //表格列表头start
        tableCols:[
          {label:'接口标识',prop:'apiCode',align:'center'},
          {label:'接口名称',prop:'apiName',align:'center'},
          {label:'接口功能',prop:'apiFunction',align:'center'},
          {label:'接口连接',prop:'apiUrl',align:'center'},
          {label:'所属菜单',prop:'menuName',align:'center'},
          {label:'访问规则',prop:'checkRule',align:'center',formatter:this.commonUtil.getCodeName},
          {label:'操作',prop:'operation',align:'center',type:'button',btnList:[
            {label:'查看',type:'text',auth:'sysApi_getdetail',handle:(that,row)=>this.showDetail({pageData:this.pageData,param:{id:row.id},callback:this.getDetailBack})},
            {label:'编辑',type:'text',auth:'sysApi_update',handle:(that,row)=>this.showEdit({pageData:this.pageData,param:{id:row.id},removeEmpty:false,callback:this.getDetailBack})},
            {label:'删除',type:'text',auth:'sysApi_delete',handle:(that,row)=>this.delete({pageData:this.pageData,param:{id:row.id}})},
          ]}
        ],
        //表格列表头end
        //modal配置 start
        modalConfig:{ 
          title: "新增", //弹窗标题,值为:新增，查看，编辑
          show: false, //弹框显示
          formEditDisabled:false,//编辑弹窗是否可编辑
          width:'700px',//弹出框宽度
          type:'1',//类型 1新增 2编辑 3详情
          removeEmpty:false,//空字符串是否作为参数传递到后台 false:空字符串将传递到后台 true:空字符串将不会传递到后台，后台对应的值为null
          submitUrl:'',//提交按钮对应的url
          disabled:false,//按钮是否可用
          modalRef:"modalRef",//modal标识
        },
        //modal配置 end
        //modal表单 start
        modalForm:[
          {type:'Input',label:'接口标识',prop:'apiCode',rules:{required:true,maxLength:40}},
          {type:'Input',label:'接口名称',prop:'apiName',rules:{required:true,maxLength:40}},
          {type:'Input',label:'接口功能',prop:'apiFunction',rules:{required:true,maxLength:200}},
          {type:'Input',label:'接口连接',prop:'apiUrl',rules:{required:true,maxLength:200}},
          {type:'Select',label:'访问规则',prop:'checkRule',rules:{required:true},options:this.selectUtil.checkRule},
        ],
        //modal表单 end
        //modal 数据 start
        modalData : {//modal页面数据
          menuId:"",//菜单表主键 
          apiCode:"",//接口唯一标识 
          apiName:"",//接口名称 
          apiFunction:"",//接口功能 
          apiUrl:"",//接口连接 
          isDefault:"",//是否默认 1是 2否 
          checkRule:"",//拦截规则
        },
        //modal 数据 end
        //modal 按钮 start
        modalHandles:[
          {label:'取消',type:'default',handle:()=>this.closeModal(this.pageData)},
          {label:'提交',type:'primary',handle:()=>this.saveData()}
        ],
        //modal 按钮 end
        //其他参数 start
        roles:[],//角色
        //其他参数 end
      }
    }
  },
  mounted(){
    this.pageData.queryData.menuId = this.$route.query['menuId'];
    this.searchtablelist(this.pageData);
  },
  methods:{
    ...mapActions([
      'searchtablelist','showModal','save','closeModal','showDetail','delete','saveCallback','showDetailCallback',
      'showEdit','showEditCallback','showInsert','deleteCallback','selectChange','deleteBatch','reset'
    ]),
    saveData(){
      this.pageData.modalData.menuId = this.$route.query['menuId'];
      this.save({pageData:this.pageData});
    }
  }
};