import { mapState, mapGetters, mapActions, mapMutations } from 'vuex'
export default {
  name:'sysUser',
  data() {
    return{
      pageData:{
        that:this,
        //请求的url start
        requestUrl:{
          listApi:"/api/reportDatasource/getTableList",//获取表格数据api
          insertApi:"/api/reportDatasource/insert",//新增用api
          updateApi:"/api/reportDatasource/update",//更新用api
          getDetailApi:"/api/reportDatasource//getDetail",//获取详情用api
          deleteOneApi:"/api/reportDatasource/delete",//单条删除api
          deleteBatchApi:"/api/reportDatasource/deletebatch",//批量删除api
        },
        //请求的url end
        //查询表单内容 start
        searchForm:[
          {type:'Input',label:'数据库代码',prop:'code'},
          {type:'Input',label:'数据库名称',prop:'name'},
          {type:'Select',label:'数据驱动',prop:'driverClass',options:this.selectUtil.driverClass},
        ],
        //查询表单内容 end
        //查询条件 start
        queryData:{
          code:"",//数据库代码
          name:"",//数据库名称
          driverClass:"",//数据驱动
        },
        //查询条件 end
        //查询表单按钮start
        searchHandle:[
          {label:'查询',type:'primary',handle:()=>this.searchtablelist(this.pageData),auth:'sysApi_search'},
          {label:'重置',type:'warning',handle:()=>this.reset(this.pageData),auth:'sysApi_search'}
        ],
        //查询表单按钮end
        //表格数据start
        tableData:[],
        //表格数据end
        //表格工具栏按钮 start
        tableHandles:[
          {label:'新增',type:'primary',handle:()=>this.showInsert({pageData:this.pageData,removeEmpty:false,init:this.getRoles}),auth:'sysApi_insert'},
          {label:'批量删除',type:'danger',handle:()=>this.deleteBatch({pageData:this.pageData}),auth:'sysApi_batchdelete'}
        ],
        //表格工具栏按钮 end
        selectList:[],//表格选中的数据
        //表格分页信息start
        tablePage:{
          currentPage: 1,
          pageSize:10,
          pageTotal: 0,
          pageSizeRange:[5, 10, 20, 50]
        },
        //表格分页信息end
        //表格列表头start
        tableCols:[
          {label:'数据库代码',prop:'code',align:'center'},
          {label:'数据库名称',prop:'name',align:'center'},
          {label:'数据驱动',prop:'driverClass',align:'center'},
          {label:'数据库链接',prop:'jdbcUrl',align:'center',overflow:true},
          {label:'操作',prop:'operation',align:'center',type:'button',btnList:[
            {label:'查看',type:'text',auth:'sysApi_getdetail',handle:(that,row)=>this.showDetail({pageData:this.pageData,param:{id:row.id},callback:this.getDetailBack})},
            {label:'编辑',type:'text',auth:'sysApi_update',handle:(that,row)=>this.showEdit({pageData:this.pageData,param:{id:row.id},removeEmpty:false,callback:this.getDetailBack})},
            {label:'删除',type:'text',auth:'sysApi_delete',handle:(that,row)=>this.delete({pageData:this.pageData,param:{id:row.id}})},
          ]}
        ],
        //表格列表头end
        //modal配置 start
        modalConfig:{ 
          title: "新增", //弹窗标题,值为:新增，查看，编辑
          show: false, //弹框显示
          formEditDisabled:false,//编辑弹窗是否可编辑
          width:'700px',//弹出框宽度
          type:'1',//类型 1新增 2编辑 3详情
          removeEmpty:false,//空字符串是否作为参数传递到后台 false:空字符串将传递到后台 true:空字符串将不会传递到后台，后台对应的值为null
          submitUrl:'',//提交按钮对应的url
          disabled:false,//按钮是否可用
          modalRef:"modalRef",//modal标识
        },
        //modal配置 end
        //modal表单 start
        modalForm:[
          {type:'Input',label:'数据库代码',prop:'code',rules:{required:true,maxLength:40}},
          {type:'Input',label:'数据库名称',prop:'name',rules:{required:true,maxLength:40}},
          {type:'Select',label:'数据驱动',prop:'driverClass',rules:{required:true},options:this.selectUtil.driverClass},
          {type:'Input',label:'数据库链接',prop:'jdbcUrl',rules:{required:true,maxLength:500}},
          {type:'Input',label:'登录名',prop:'user',rules:{required:true,maxLength:40}},
          {type:'Input',label:'密码',prop:'password',rules:{required:true,maxLength:50}},
        ],
        //modal表单 end
        //modal 数据 start
        modalData : {//modal页面数据
          code:"",//接口唯一标识 
          name:"",//接口名称 
          driverClass:"",//接口功能 
          jdbcUrl:"",//接口连接 
          user:"",//是否默认 1是 2否 
          password:"",//拦截规则
        },
        //modal 数据 end
        //modal 按钮 start
        modalHandles:[
          {label:'取消',type:'default',handle:()=>this.closeModal(this.pageData)},
          {label:'提交',type:'primary',handle:()=>this.save({pageData:this.pageData})}
        ],
        //modal 按钮 end
        //其他参数 start
        roles:[],//角色
        //其他参数 end
      }
    }
  },
  mounted(){
    this.searchtablelist(this.pageData);
  },
  methods:{
    ...mapActions([
      'searchtablelist','showModal','save','closeModal','showDetail','delete','saveCallback','showDetailCallback',
      'showEdit','showEditCallback','showInsert','deleteCallback','selectChange','deleteBatch','reset'
    ]),
  }
};