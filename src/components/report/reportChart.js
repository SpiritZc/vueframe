import { mapState, mapGetters, mapActions, mapMutations } from 'vuex'
export default {
  name:'reportChartx',
  data() {
    return{
      pageData:{
        that:this,
        //请求的url start
        requestUrl:{
          listApi:"/api/tplChartX/getTableList",//获取表格数据api
          insertApi:"/api/tplChartX/insert",//新增用api
          updateApi:"/api/tplChartX/update",//更新用api
          getDetailApi:"/api/tplChartX//getDetail",//获取详情用api
          deleteOneApi:"/api/tplChartX/delete",//单条删除api
          deleteBatchApi:"/api/tplChartX/deletebatch",//批量删除api
          getCellsApi:"/api/reportCell/getDatabaseCells",//获取所有变量单元格
        },
        //请求的url end
        //查询表单内容 start
        searchForm:[
          {type:'Input',label:'图表名称',prop:'chartName'},
          {type:'Select',label:'图表类型',prop:'chartType',options:this.selectUtil.chartType,props:this.commonConstants.props,},
        ],
        //查询表单内容 end
        //查询条件 start
        queryData:{
            chartName:"",
            chartType:"",
        },
        //查询条件 end
        //查询表单按钮start
        searchHandle:[
          {label:'查询',type:'primary',handle:()=>this.searchtablelist(this.pageData),auth:'sysApi_search'},
          {label:'重置',type:'warning',handle:()=>this.reset(this.pageData),auth:'sysApi_search'}
        ],
        //查询表单按钮end
        //表格数据start
        tableData:[],
        //表格数据end
        //表格工具栏按钮 start
        tableHandles:[
          {label:'新增',type:'primary',handle:()=>this.showInsert({pageData:this.pageData,removeEmpty:false,init:this.getCells}),auth:'sysApi_insert'},
          {label:'批量删除',type:'danger',handle:()=>this.deleteBatch({pageData:this.pageData}),auth:'sysApi_batchdelete'}
        ],
        //表格工具栏按钮 end
        selectList:[],//表格选中的数据
        //表格分页信息start
        tablePage:{
          currentPage: 1,
          pageSize:10,
          pageTotal: 0,
          pageSizeRange:[5, 10, 20, 50]
        },
        //表格分页信息end
        //表格列表头start
        tableCols:[
          {label:'图表名称',prop:'chartName',align:'center'},
          {label:'图表类型',prop:'chartType',align:'center'},
          {label:'x轴变量',prop:'chartXAttr',align:'center',overflow:true},
          {label:'分类属性',prop:'series',align:'center',overflow:true},
          {label:'分类属性名称',prop:'seriesName',align:'center',overflow:true},
          {label:'操作',prop:'operation',align:'center',type:'button',btnList:[
            {label:'查看',type:'text',auth:'sysApi_getdetail',handle:(that,row)=>this.showDetail({pageData:this.pageData,param:{id:row.id},callback:this.getDetailBack})},
            {label:'编辑',type:'text',auth:'sysApi_update',handle:(that,row)=>this.showEdit({pageData:this.pageData,param:{id:row.id},removeEmpty:false,callback:this.getDetailBack})},
            {label:'删除',type:'text',auth:'sysApi_delete',handle:(that,row)=>this.delete({pageData:this.pageData,param:{id:row.id}})},
          ]}
        ],
        //表格列表头end
        //modal配置 start
        modalConfig:{ 
          title: "新增", //弹窗标题,值为:新增，查看，编辑
          show: false, //弹框显示
          formEditDisabled:false,//编辑弹窗是否可编辑
          width:'700px',//弹出框宽度
          type:'1',//类型 1新增 2编辑 3详情
          removeEmpty:false,//空字符串是否作为参数传递到后台 false:空字符串将传递到后台 true:空字符串将不会传递到后台，后台对应的值为null
          submitUrl:'',//提交按钮对应的url
          disabled:false,//按钮是否可用
          modalRef:"modalRef",//modal标识
        },
        //modal配置 end
        //modal表单 start
        modalForm:[
          {type:'Input',label:'图表名称',prop:'chartName',rules:{required:true,maxLength:40}},
          {type:'Select',label:'图表类型',prop:'chartType',rules:{required:true},options:this.selectUtil.chartType},
          {type:'Select',label:'X轴变量',prop:'chartXAttr',rules:{required:true},props:{label:"cellValue",value:"id"}},
          {type:'Select',label:'分类属性',width:"400px",prop:'series',rules:{required:true},props:{label:"cellValue",value:"id"},multiple:true},
          {type:'Input',label:'属性名称',width:"400px",prop:'seriesName',rules:{required:true},suggestions:"多个属性名称用|分隔，顺序和个数需与分类属性一致。"},
        ],
        //modal表单 end
        //modal 数据 start
        modalData : {//modal页面数据
            chartName:"",//图表名称
            chartType:"",//图表类型 
            chartXAttr:"",//X轴变量
            series:[],//分类属性
            seriesName:"",//属性名称
        },
        //modal 数据 end
        //modal 按钮 start
        modalHandles:[
          {label:'取消',type:'default',handle:()=>this.closeModal(this.pageData)},
          {label:'提交',type:'primary',handle:()=>this.saveData()}
        ],
        //modal 按钮 end
      }
    }
  },
  mounted(){
    this.searchtablelist(this.pageData);
  },
  methods:{
    ...mapActions([
      'searchtablelist','showModal','save','closeModal','showDetail','delete','saveCallback','showDetailCallback',
      'showEdit','showEditCallback','showInsert','deleteCallback','selectChange','deleteBatch','reset'
    ]),
    getCells(){//获取所有的变量单元格
        let id = this.$route.query['id'];
        let param = {
            url:this.pageData.requestUrl.getCellsApi,
            params:{tplId:id},
            removeEmpty:false,
            callback:this.getCellsCallback
          }
          this.commonUtil.doAjaxPost(param)
    },
    getCellsCallback(data)
    {
        this.$refs['modalRef'].modalForm[2].options = data.responseData;
        this.$refs['modalRef'].modalForm[3].options = data.responseData;
        this.$refs['modalRef'].$forceUpdate();//在methods中需强制更新，mounted中不需要
    },
    saveData(){
        this.pageData.modalData.tplId =  this.$route.query['id'];
        this.save({pageData:this.pageData});
    }
  }
};