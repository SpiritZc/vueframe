import { mapState, mapGetters, mapActions, mapMutations } from 'vuex'
export default {
  name:'reportTpl',
  data() {
    return{
      pageData:{
        that:this,
        //请求的url start
        requestUrl:{
          listApi:"/api/reportTpl/getTableList",//获取表格数据api
          insertApi:"/api/reportTpl/insert",//新增用api
          updateApi:"/api/reportTpl/update",//更新用api
          getDetailApi:"/api/reportTpl//getDetail",//获取详情用api
          deleteOneApi:"/api/reportTpl/delete",//单条删除api
          deleteBatchApi:"/api/reportTpl/deletebatch",//批量删除api
          getDataSourceApi:"/api/reportDatasource/getAllDataSource",//获取数据源api
        },
        //请求的url end
        //查询表单内容 start
        searchForm:[
          {type:'Input',label:'报表代码',prop:'tplCode'},
          {type:'Input',label:'报表名称',prop:'tplName'},
        ],
        //查询表单内容 end
        //查询条件 start
        queryData:{
            tplCode:"",
            tplName:"",
        },
        //查询条件 end
        //查询表单按钮start
        searchHandle:[
          {label:'查询',type:'primary',handle:()=>this.searchtablelist(this.pageData),auth:'sysApi_search'},
          {label:'重置',type:'warning',handle:()=>this.reset(this.pageData),auth:'sysApi_search'}
        ],
        //查询表单按钮end
        //表格数据start
        tableData:[],
        //表格数据end
        //表格工具栏按钮 start
        tableHandles:[
          {label:'新增',type:'primary',handle:()=>this.showInsert({pageData:this.pageData,removeEmpty:false,init:this.getDatasource}),auth:'sysApi_insert'},
          {label:'批量删除',type:'danger',handle:()=>this.deleteBatch({pageData:this.pageData}),auth:'sysApi_batchdelete'}
        ],
        //表格工具栏按钮 end
        selectList:[],//表格选中的数据
        //表格分页信息start
        tablePage:{
          currentPage: 1,
          pageSize:10,
          pageTotal: 0,
          pageSizeRange:[5, 10, 20, 50]
        },
        //表格分页信息end
        //表格列表头start
        tableCols:[
          {label:'模板代码',prop:'tplCode',align:'center'},
          {label:'模板名称',prop:'tplName',align:'center'},
          // {label:'sql语句',prop:'tplSql',align:'center',overflow:true},
          // {label:'参数',prop:'tplParam',align:'center',overflow:true},
          {label:'数据库代码',prop:'dataSourceCode',align:'center'},
          {label:'数据库名称',prop:'dataSourceName',align:'center'},
          {label:'操作',prop:'operation',align:'center',type:'button',btnList:[
            {label:'查看',type:'text',auth:'sysApi_getdetail',handle:(that,row)=>this.showDetail({pageData:this.pageData,param:{id:row.id},init:this.getDatasource})},
            {label:'编辑',type:'text',auth:'sysApi_update',handle:(that,row)=>this.showEdit({pageData:this.pageData,param:{id:row.id},removeEmpty:false,init:this.getDatasource})},
            {label:'删除',type:'text',auth:'sysApi_delete',handle:(that,row)=>this.delete({pageData:this.pageData,param:{id:row.id}})},
            {label:'编辑报表',type:'text',auth:'sysApi_update',handle:(that,row)=>this.editTpl(row.id)},
            {label:'图表设计',type:'text',auth:'sysApi_update',handle:(that,row)=>this.chartManage(row.id)},
            {label:'生成报表',type:'text',auth:'sysApi_update',handle:(that,row)=>this.generateTpl(row.id)},
          ]}
        ],
        //表格列表头end
        //modal配置 start
        modalConfig:{ 
          title: "新增", //弹窗标题,值为:新增，查看，编辑
          show: false, //弹框显示
          formEditDisabled:false,//编辑弹窗是否可编辑
          width:'700px',//弹出框宽度
          type:'1',//类型 1新增 2编辑 3详情
          removeEmpty:false,//空字符串是否作为参数传递到后台 false:空字符串将传递到后台 true:空字符串将不会传递到后台，后台对应的值为null
          submitUrl:'',//提交按钮对应的url
          disabled:false,//按钮是否可用
          modalRef:"modalRef",//modal标识
        },
        //modal配置 end
        //modal表单 start
        modalForm:[
          {type:'Input',label:'模板代码',prop:'tplCode',rules:{required:true,maxLength:40}},
          {type:'Input',label:'模板名称',prop:'tplName',rules:{required:true,maxLength:40}},
          {type:'Select',label:'报表数据库',prop:'dataSource',rules:{required:true},props:{label:"name",value:"id"},multiple:true},
        ],
        //modal表单 end
        //modal 数据 start
        modalData : {//modal页面数据
          tplCode:"",//模板代码
          tplName:"",//模板名称
          dataSource:[],//数据源
        },
        //modal 数据 end
        //modal 按钮 start
        modalHandles:[
          {label:'取消',type:'default',handle:()=>this.closeModal(this.pageData)},
          {label:'提交',type:'primary',handle:()=>this.save({pageData:this.pageData})}
        ],
        //modal 按钮 end
      }
    }
  },
  mounted(){
    this.searchtablelist(this.pageData);
  },
  methods:{
    ...mapActions([
      'searchtablelist','showModal','save','closeModal','showDetail','delete','saveCallback','showDetailCallback',
      'showEdit','showEditCallback','showInsert','deleteCallback','selectChange','deleteBatch','reset'
    ]),
    getDatasource:function(){
        //获取数据库
        let param = {
            url:this.pageData.requestUrl.getDataSourceApi,
            params:{},
            removeEmpty:false,
            callback:this.getDataSourceCallback
          }
          this.commonUtil.doAjaxPost(param)
    },
    getDataSourceCallback:function(response){
        this.$refs['modalRef'].modalForm[2].options = response.responseData;
        this.$refs['modalRef'].$forceUpdate();//在methods中需强制更新，mounted中不需要
    },
    editTpl(id){
      this.$router.push({ name:"report",query:{id:id}})
    },
    generateTpl(id){
      this.$router.push({ name:"generateReport",query:{id:id}})
    },
    chartManage(id){
      this.$router.push({ name:"chartManage",query:{id:id}})
    },
  }
};