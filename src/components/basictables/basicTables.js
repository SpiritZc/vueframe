import { mapState, mapGetters, mapActions, mapMutations } from 'vuex'
export default {
  name:'basicTables',
  data() {
    return{
      pageData:{
        that:this,
        //请求的url start
        requestUrl:{
          listApi:"/api/basicDatabase/getDatabaseTables",//获取表格数据api
          getProjectList:"/api/basicProject/getAllProjects",//获取所有的项目信息
          getTemplateVariables:"/api/basicTemplateVariable/getTemplateVariables",//获取模板变量
          generateApi:"/api/basicTemplate/generateFile",//生成代码用api
          getTemplateTypeApi:"/api/basicTemplateType/gettemplateTypeByProjectId",//获取项目模板类型
        },
        //请求的url end
        //查询表单内容 start
        searchForm:[
          {type:'Input',label:'表名称',prop:'tableName'},
        ],
        //查询表单内容 end
        //查询条件 start
        queryData:{
          projectId:"",
          tableName:"",  
        },
        //查询条件 end
        //查询表单按钮start
        searchHandle:[
          {label:'查询',type:'primary',handle:()=>this.getTableDatas(),auth:'basicTables_search'},
          {label:'重置',type:'warning',handle:()=>this.resetSearch(),auth:'basicTables_search'}
        ],
        //查询表单按钮end
        //表格数据start
        tableData:[],
        //表格数据end
        //表格工具栏按钮 start
        tableHandles:[
        ],
        //表格工具栏按钮 end
        selectList:[],//表格选中的数据
        //表格分页信息start
        tablePage:{
          currentPage: 1,
          pageSize:10,
          pageTotal: 0,
          pageSizeRange:[5, 10, 20, 50]
        },
        //表格分页信息end
        //表格列表头start
        tableCols:[
          {label:'数据库代码',prop:'databaseCode',align:'center'},
          {label:'表名',prop:'tableName',align:'center'},
          {label:'表类型',prop:'tableType',align:'center'},
          {label:'操作',prop:'operation',align:'center',type:'button',btnList:[
          {label:'生成代码',type:'text',auth:'basicTables_generate',handle:(that,row)=>this.generate(row.tableName)},
          ]}
        ],
        //表格列表头end
        //modal配置 start
        modalConfig:{ 
          title: "生成代码", //弹窗标题,值为:新增，查看，编辑
          show: false, //弹框显示
          formEditDisabled:false,//编辑弹窗是否可编辑
          width:'700px',//弹出框宽度
          type:'1',//类型 1新增 2编辑 3详情
          removeEmpty:false,//空字符串是否作为参数传递到后台 false:空字符串将传递到后台 true:空字符串将不会传递到后台，后台对应的值为null
          submitUrl:'',//提交按钮对应的url
          disabled:false,//按钮是否可用
          modalRef:"modalRef",//modal标识
        },
        //modal配置 end
        //modal表单 start
        modalForm:[
          {type:'Upload',label:'模板文件',tips:'上传模板文件',width:'500px',prop:'fileList',multiple:false,rules:{required:true},accept:"*.*"},
          {type:'Select',label:'所属项目',prop:'projectId',rules:{required:true},props:{label:'projectName',value:'id'},change:this.getTemplateType},
          {type:'Select',label:'模板类型',prop:'templateType',rules:{required:true},props:{label:'typeName',value:'id'},},
          {type:'Input',label:'文件路径',prop:'templateFolderpath',rules:{required:true,maxLength:200}},
        ],
        //modal表单 end
        //modal 数据 start
        modalData : {//modal页面数据
          databaseId:"",
          projectId:"",
          templateVariables:[],
          templateTypeList:[],
          tableName:"",
        },
        //modal 数据 end
        //modal 按钮 start
        modalHandles:[
          {label:'取消',type:'default',handle:()=>this.closeModal(this.pageData)},
          {label:'提交',type:'primary',handle:()=>this.saveData({pageData:this.pageData})}
        ],
        //modal 按钮 end
        //其他参数 start
        templateTypeList:[],//模板类型
        //其他参数 end
      }
    }
  },
  mounted(){
    this.getTableDatas();
    this.getProject();
  },
  methods:{
    ...mapActions([
      'searchtablelist','showModal','save','closeModal','showDetail','delete','saveCallback','showDetailCallback',
      'showEdit','showEditCallback','showInsert','deleteCallback','selectChange','deleteBatch','reset'
    ]),
    getTableDatas(){
      this.pageData.queryData.projectId = this.$route.query['projectId'];
      if(this.pageData.queryData.projectId == "")
      {
          this.commonUtil.showMessage({message:this.commonUtil.getMessageFromList("error.input.notnull",['所选项目']),type: this.commonConstants.messageType.error})
          return;
      }
      // this.searchtablelist(this.pageData);
      let param = {
        url:this.pageData.requestUrl.listApi,
        params:this.pageData.queryData,
        removeEmpty:false,
        callback:this.getTableDatasCallback
      }
      this.commonUtil.doAjaxPost(param)
    },
    getTableDatasCallback(data){
      this.pageData.tableData = data.responseData;
    },
    resetSearch(){
      this.commonUtil.clearObj(this.pageData.queryData);
      this.getTableDatas();
    },
    getProject(){//获取项目下拉框信息
      let param = {
            url:this.pageData.requestUrl.getProjectList,
            params:{},
            removeEmpty:false,
            callback:this.getProjectCallback
          }
          this.commonUtil.doAjaxPost(param)
    },
    getProjectCallback(data){
      this.pageData.modalForm[1].options = data.responseData;
      this.pageData.searchForm[1].options = data.responseData;
      this.$refs['searchForm'].$forceUpdate();//在methods中需强制更新，mounted中不需要
    },
    generate(tableName){
      this.pageData.modalData.tableName = tableName;
      this.pageData.modalConfig.show=true;
      this.getTemplateVariables();
      this.getTemplateType()
    },
    //获取模板变量
    getTemplateVariables(){
      let param = {
        url:this.pageData.requestUrl.getTemplateVariables,
        params:{projectId:this.$route.query['projectId']},
        removeEmpty:false,
        callback:this.getTemplateVariablesCallback
      }
      this.commonUtil.doAjaxPost(param)
    },
    getTemplateVariablesCallback(data){
      this.pageData.modalData.templateVariables = data.responseData
    },
    //获取模板类型templateTypeList
    getTemplateType(){
      let param = {
        url:this.pageData.requestUrl.getTemplateTypeApi,
        params:{projectId:this.$route.query['projectId']},
        removeEmpty:false,
        callback:this.getTemplateTypeCallback
      }
      this.commonUtil.doAjaxPost(param)
    },
    getTemplateTypeCallback(data){
      this.pageData.templateTypeList = data.responseData;
    },
    closeDialog(){
      this.pageData.modalConfig.show = false;//隐藏弹框
      this.$refs["modalForm"].resetFields();
    },
    doGenerate(){//生成代码
      var _self = this;
      this.$refs["modalForm"].validate((valid) => {
        if (valid) {
          let params = {
            projectId:this.$route.query['projectId'],
            templateTypeList:this.pageData.modalData.templateTypeList,
            databaseId:this.$route.query['databaseId'],
            variables:this.pageData.modalData.templateVariables,
            tableName:this.pageData.modalData.tableName,
          }
          this.commonUtil.download(this.pageData.requestUrl.generateApi,params,function(response){
            _self.closeDialog();
          })
        }else{
          return false;
        }
      })
    },
  }
};